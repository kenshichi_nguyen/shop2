<?php
/**
 * Cấu hình cơ bản cho WordPress
 *
 * Trong quá trình cài đặt, file "wp-config.php" sẽ được tạo dựa trên nội dung 
 * mẫu của file này. Bạn không bắt buộc phải sử dụng giao diện web để cài đặt, 
 * chỉ cần lưu file này lại với tên "wp-config.php" và điền các thông tin cần thiết.
 *
 * File này chứa các thiết lập sau:
 *
 * * Thiết lập MySQL
 * * Các khóa bí mật
 * * Tiền tố cho các bảng database
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Thiết lập MySQL - Bạn có thể lấy các thông tin này từ host/server ** //
/** Tên database MySQL */
define( 'DB_NAME', 'linea' );
define('WP_CACHE', true);
define( 'WPCACHEHOME', 'var\www\html\shop.reginavn.com\wp-content\plugins\wp-super-cache/' );

/** Username của database */
define( 'DB_USER', 'db_user' );

/** Mật khẩu của database */
define( 'DB_PASSWORD', '4Vt<k=meXMU727?' );

/** Hostname của database */
define( 'DB_HOST', 'localhost' );

/** Database charset sử dụng để tạo bảng database. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Kiểu database collate. Đừng thay đổi nếu không hiểu rõ. */
define('DB_COLLATE', '');

/**#@+
 * Khóa xác thực và salt.
 *
 * Thay đổi các giá trị dưới đây thành các khóa không trùng nhau!
 * Bạn có thể tạo ra các khóa này bằng công cụ
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * Bạn có thể thay đổi chúng bất cứ lúc nào để vô hiệu hóa tất cả
 * các cookie hiện có. Điều này sẽ buộc tất cả người dùng phải đăng nhập lại.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         ':78la?t?koZLlH@E:V63Q!OVqUWtytPc`6K+@t70MqMi}!+wcU]AzV7;=w6%8YOw' );
define( 'SECURE_AUTH_KEY',  'CvEXvWwE~N;i;o%_DB^(,46*%P/^HM^!nU=/4!6}-(TsnCswj;f [6zw_/ oxIm/' );
define( 'LOGGED_IN_KEY',    'BglpuOdmvvz_hGwh(8)XbFf^zr(Fl,~36U;o?w!wy9}tPR9kOAEXzt%ku:a3LTR.' );
define( 'NONCE_KEY',        ':uf#F]{GKdTB)968.Ad##g[ce qOhw&1$tC?M~K>{FTrYg`Z}bJ:Lx:5o GMy%k/' );
define( 'AUTH_SALT',        '6H_$6jJ^|/9aw[b?%aPe%~qW+G66R:T-+F<V/?ER<2gS;W~YaZGG)`4,KD_/oI!k' );
define( 'SECURE_AUTH_SALT', 'XsFB,m2`Odcq?D+|TEuvn8of0a.0-UfvAlwq{(k^4gz]f* MP1}QqwMR0YQ%zrX ' );
define( 'LOGGED_IN_SALT',   'DMgWcmw;tb!h:#KkG]]):=X_xsbfFg?:doADs.>BHHcwYcZ&6aLE%I0-G),:EA`k' );
define( 'NONCE_SALT',       'X?_[7~kbod^d}aYGOg)Jiur|=3^K]@kbU,2W5D#=MwVOSW y(e_Q@L^NWZadzflA' );

/**#@-*/

/**
 * Tiền tố cho bảng database.
 *
 * Đặt tiền tố cho bảng giúp bạn có thể cài nhiều site WordPress vào cùng một database.
 * Chỉ sử dụng số, ký tự và dấu gạch dưới!
 */
$table_prefix = 'sma_';

/**
 * Dành cho developer: Chế độ debug.
 *
 * Thay đổi hằng số này thành true sẽ làm hiện lên các thông báo trong quá trình phát triển.
 * Chúng tôi khuyến cáo các developer sử dụng WP_DEBUG trong quá trình phát triển plugin và theme.
 *
 * Để có thông tin về các hằng số khác có thể sử dụng khi debug, hãy xem tại Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Đó là tất cả thiết lập, ngưng sửa từ phần này trở xuống. Chúc bạn viết blog vui vẻ. */

/** Đường dẫn tuyệt đối đến thư mục cài đặt WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Thiết lập biến và include file. */
require_once(ABSPATH . 'wp-settings.php');
