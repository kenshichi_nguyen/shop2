<div class="container">
 <div class="row">
     <?php linea_home_side_banner();?>
     <?php linea_home_page_banner(); ?>
  </div>
</div>


<div class="main-container col2-left-layout">
    <div class="container">
      <div class="row">
        <div class="col-sm-9 col-sm-push-3"> 

          <?php linea_home_page_promotion_banners();?>
          <?php linea_new_products(); ?>
          <?php linea_bestseller_products(); ?>
          <?php linea_featured_products();?>

        </div>
        <aside class="col-left sidebar col-sm-3 col-xs-12 col-sm-pull-9">
	        <?php linea_home_custom_slider_wrap();?>
	        <?php linea_hotdeal_product();?>
	        <?php linea_testimonials();?>
          <?php linea_home_bottom_banner(); ?>
        </aside>
      </div>
    </div>
</div>

<?php linea_home_blog_posts();?>
<?php linea_home_customsection();?>
   
  