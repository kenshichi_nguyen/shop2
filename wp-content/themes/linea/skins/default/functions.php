<?php
if (!function_exists('linea_currency_language')) {
    function linea_currency_language()
    {
        global $linea_Options;

        if (isset($linea_Options['enable_header_language']) && ($linea_Options['enable_header_language'] != 0)) { ?>
            <div class="dropdown block-language-wrapper">
                <a role="button" data-toggle="dropdown" data-target="#" class="block-language dropdown-toggle" href="#">
                    <img src="<?php echo esc_url(LINEA_THEME_URI); ?>/images/english.png"
                         alt="<?php esc_attr_e('English', 'linea'); ?>">
                    <?php esc_attr_e('English ', 'linea'); ?><span class="caret"></span>
                </a>
                <ul class="dropdown-menu" role="menu">
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#"><img
                                    src="<?php echo esc_url(LINEA_THEME_URI); ?>/images/english.png"
                                    alt="<?php esc_attr_e('English', 'linea'); ?>"> <?php esc_attr_e('English', 'linea'); ?>
                        </a></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#"><img
                                    src="<?php echo esc_url(LINEA_THEME_URI); ?>/images/francais.png"
                                    alt="<?php esc_attr_e('French', 'linea'); ?>"> <?php esc_attr_e('French', 'linea'); ?>
                        </a></li>
                    <!--<li role="presentation"><a role="menuitem" tabindex="-1" href="#"><img src="<?php /*echo esc_url(LINEA_THEME_URI) ;*/ ?>/images/german.png" alt="<?php /*esc_attr_e('German', 'linea');*/ ?>">   <?php /*esc_attr_e('German', 'linea');*/ ?></a></li>-->
                </ul>
            </div>
            <?php
        }
        if (isset($linea_Options['enable_header_currency']) && ($linea_Options['enable_header_currency'] != 0)) {

            global $wp, $wp_query;
            $cururl = linea_curPageURL();
            if (class_exists('WOOCS')) {
                $vnd_url = add_query_arg(array("currency" => "VND"), $cururl);
                $usd_url = add_query_arg(array("currency" => "USD"), $cururl);

                $currency_selected = get_option('woocommerce_currency');

            } else {
                $vnd_url = "#";
                $usd_url = "#";
                $currency_selected = "VND";
            }

            if ((isset($_GET['currency']) && $_GET['currency'] == 'USD') || $currency_selected == 'USD') {

                $elected_currency = '<a role="button" data-toggle="dropdown" data-target="#" class="block-currency dropdown-toggle" href="' . esc_url($usd_url) . '">'
                    . esc_attr("USD", "linea") . ' <span class="caret"></span></a>';

            } else {
                $elected_currency = '<a role="button" data-toggle="dropdown" data-target="#" class="block-currency dropdown-toggle" href="' . esc_url($vnd_url) . '">'
                    . esc_attr("VND", "linea") . ' <span class="caret"></span></a>';
            }
            ?>

            <div class="dropdown block-currency-wrapper">
                <?php echo wp_specialchars_decode($elected_currency); ?>
                <ul class="dropdown-menu" role="menu">
                    <li role="presentation">
                        <a role="menuitem" tabindex="-1" href="<?php echo esc_url($vnd_url); ?>">
                            <?php esc_attr_e('đ - VND', 'linea'); ?>
                        </a>
                    </li>

                    <li role="presentation">
                        <a role="menuitem" tabindex="-1" href="<?php echo esc_url($usd_url); ?>">
                            <?php esc_attr_e('$ - Dollar', 'linea'); ?>
                        </a>
                    </li>
                </ul>
            </div>
            <?php
        }
    }
}


if (!function_exists('linea_msg')) {
    function linea_msg()
    {
        global $linea_Options;

        if (is_user_logged_in()) {
            global $current_user;

            if (isset($linea_Options['welcome_msg']) && ($linea_Options['welcome_msg'] != '')) {
                echo esc_attr_e('Logged in as', 'linea') . '   <b>' . esc_attr($current_user->display_name) . '</b>';
            }
        } else {
            if (isset($linea_Options['welcome_msg']) && ($linea_Options['welcome_msg'] != '')) {
                echo wp_specialchars_decode($linea_Options['welcome_msg']);
            }
        }
    }
}

if (!function_exists('linea_logo_image')) {
    function linea_logo_image()
    {
        global $linea_Options;

        $logoUrl = LINEA_THEME_URI . '/images/logo.png';

        if (isset($linea_Options['header_use_imagelogo']) && $linea_Options['header_use_imagelogo'] == 0) { ?>
            <a class="logo logotext logo-title" title="<?php bloginfo('name'); ?>"
               href="<?php echo esc_url(get_home_url()); ?> ">
                <?php bloginfo('name'); ?>
            </a>
            <?php
        } else if (isset($linea_Options['header_logo']['url']) && !empty($linea_Options['header_logo']['url'])) {
            $logoUrl = $linea_Options['header_logo']['url'];
            ?>
            <a class="logo" title="<?php bloginfo('name'); ?>" href="<?php echo esc_url(get_home_url()); ?> "> <img
                        alt="<?php bloginfo('name'); ?>" src="<?php echo esc_url($logoUrl); ?>"
                        height="<?php echo !empty($linea_Options['header_logo_height']) ? esc_html($linea_Options['header_logo_height']) : ''; ?>"
                        width="<?php echo !empty($linea_Options['header_logo_width']) ? esc_html($linea_Options['header_logo_width']) : ''; ?>">
            </a>
            <?php
        } else { ?>
            <a class="logo" title="<?php bloginfo('name'); ?>" href="<?php echo esc_url(get_home_url()); ?> ">
                <img src="<?php echo esc_url($logoUrl); ?>" alt="<?php bloginfo('name'); ?>"> </a>
        <?php }

    }
}

if (!function_exists('linea_mobile_search')) {
    function linea_mobile_search()
    {
        global $linea_Options;
        $Linea = new Linea();
        if (isset($linea_Options['header_remove_header_search']) && !$linea_Options['header_remove_header_search']) :
            echo '<div class="mobile-search">';
            echo linea_search_form();
            echo '<div class="search-autocomplete" id="search_autocomplete1" style="display: none;"></div></div>';
        endif;
    }
}

if (!function_exists('linea_search_form')) {
    function linea_search_form()
    {
        global $linea_Options;
        $Linea = new Linea();
        ?>
        <?php if (isset($linea_Options['header_remove_header_search']) && !$linea_Options['header_remove_header_search']) : ?>
        <?php if (class_exists('MGK_Front_WooAjaxSearch') && isset($linea_Options['header_show_ajax_search']) && $linea_Options['header_show_ajax_search'] == 1) {


            echo do_shortcode('[mgk_woocommerce_ajax_search]');


        } else { ?>
            <form name="myform" method="GET" action="<?php echo esc_url(home_url('/')); ?>">
                <input class="mgksearch" type="text" value="<?php echo get_search_query(); ?>"
                       placeholder="<?php esc_attr_e('Search entire store here...', 'linea'); ?>" maxlength="70"
                       name="s">
                <?php if (class_exists('WooCommerce')) : ?>
                    <input type="hidden" value="product" name="post_type">
                <?php endif; ?>
                <button class="search-btn-bg" type="submit">&nbsp;</button>

            </form>


        <?php }endif; ?>
        <?php
    }
}


if (!function_exists('linea_home_page_banner')) {
    function linea_home_page_banner()
    {
        global $linea_Options;

        ?>

        <div class="col-md-9 col-sm-9 col-xs-12 home-slider">
            <div id="magik-slideshow" class="magik-slideshow slider-block">


                <?php if (isset($linea_Options['enable_home_gallery']) && $linea_Options['enable_home_gallery'] && isset($linea_Options['home-page-slider']) && !empty($linea_Options['home-page-slider'])) { ?>

                    <div id='rev_slider_4_wrapper' class='rev_slider_wrapper fullwidthbanner-container'>
                        <div id='rev_slider_4' class='rev_slider fullwidthabanner'>
                            <ul>
                                <?php foreach ($linea_Options['home-page-slider'] as $slide) : ?>

                                    <li data-transition='random' data-slotamount='7' data-masterspeed='1000'
                                        data-thumb='<?php echo esc_url($slide['thumb']); ?>'>
                                        <img
                                                src="<?php echo esc_url($slide['image']); ?>"
                                                data-data-bgposition='left top' data-bgfit='cover'
                                                data-bgrepeat='no-repeat'
                                                alt="<?php echo esc_url($slide['image']); ?>"/>
                                        <?php echo wp_specialchars_decode($slide['description']); ?>
                                        <a class="s-link"
                                           href="<?php echo !empty($slide['url']) ? esc_url($slide['url']) : '#' ?>"></a>


                                    </li>

                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>

                <?php } ?>
            </div>
        </div>

        <?php

    }
}

if (!function_exists('linea_home_side_banner')) {
    function linea_home_side_banner()
    {
        global $linea_Options;
        if (isset($linea_Options['enable_home_side_banner']) && $linea_Options['enable_home_side_banner']) {
            ?>
            <div class="col-md-3 col-md-4 col-sm-3 hidden-xs">
                <div class="side-banner">

                    <a href="<?php echo !empty($linea_Options['home-side-banner-url']) ? esc_url($linea_Options['home-side-banner-url']) : '#' ?>"
                       title="<?php echo esc_html($linea_Options['home-side-banner_title']); ?>">
                        <?php if (isset($linea_Options['home-side-banner-text']) && !empty($linea_Options['home-side-banner-text'])) { ?>
                            <?php echo wp_specialchars_decode($linea_Options['home-side-banner-text']); ?>
                        <?php } ?>
                        <img class="hidden-xs" src="<?php echo esc_url($linea_Options['home-side-banner']['url']); ?>"
                             alt="<?php echo esc_html($linea_Options['home-side-banner_title']); ?>"></a>


                </div>
            </div>

        <?php } ?>
        <?php
    }
}


if (!function_exists('linea_home_bottom_banner')) {
    function linea_home_bottom_banner()
    {
        global $linea_Options;
        if (isset($linea_Options['enable_home_bottom_banner']) && $linea_Options['enable_home_bottom_banner']) {
            ?>
            <div class="featured-add-box">
                <div class="featured-add-inner">

                    <a href="<?php echo !empty($linea_Options['home-bottom-banner-url']) ? esc_url($linea_Options['home-bottom-banner-url']) : '#' ?>"
                       title="<?php echo esc_html($linea_Options['home-bottom-banner_title']); ?>">

                        <img src="<?php echo esc_url($linea_Options['home-bottom-banner']['url']); ?>"
                             alt="<?php echo esc_html($linea_Options['home-bottom-banner_title']); ?>"></a>

                    <?php if (isset($linea_Options['home-bottom-banner-text']) && !empty($linea_Options['home-bottom-banner-text'])) { ?>
                        <div class="banner-content">
                            <?php echo wp_specialchars_decode($linea_Options['home-bottom-banner-text']); ?>
                        </div>
                    <?php } ?>

                </div>
            </div>
        <?php } ?>
        <?php
    }
}


if (!function_exists('linea_footer_signupform')) {
    function linea_footer_signupform()
    {
        global $linea_Options;
        if (isset($linea_Options['enable_mailchimp_form']) && !empty($linea_Options['enable_mailchimp_form'])) {
            if (function_exists('mc4wp_show_form')) {
                ?>
                <div class="newsletter-wrap">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="newsletter">

                                    <?php
                                    mc4wp_show_form();
                                    ?>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            }
        }
    }
}

if (!function_exists('linea_footer_middle')) {
    function linea_footer_middle()
    {
        global $linea_Options;

        if (isset($linea_Options['enable_footer_middle']) && !empty($linea_Options['enable_footer_middle'])) {
            ?>

            <?php echo wp_specialchars_decode($linea_Options['footer_middle']); ?>

            <?php
        }
    }
}

if (!function_exists('linea_header_service')) {
    function linea_header_service()
    {

        global $linea_Options;

        if (isset($linea_Options['header_show_info_banner']) && !empty($linea_Options['header_show_info_banner'])) :
            ?>

            <div class="our-features-box top_header">
                <div class="container">
                    <div class="row">

                        <?php if (!empty($linea_Options['header_shipping_banner'])) : ?>
                            <div class="col-lg-3 col-xs-12 col-sm-6">
                                <div class="feature-box first"><span class="fa fa-truck"></span>
                                    <div class="content">
                                        <h3><?php echo wp_specialchars_decode($linea_Options['header_shipping_banner']); ?></h3>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php if (!empty($linea_Options['header_customer_support_banner'])) : ?>
                            <div class="col-lg-3 col-xs-12 col-sm-6">
                                <div class="feature-box"><span class="fa fa-headphones"></span>
                                    <div class="content">
                                        <h3><?php echo wp_specialchars_decode($linea_Options['header_customer_support_banner']); ?></h3>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php if (!empty($linea_Options['header_returnservice_banner'])) : ?>
                            <div class="col-lg-3 col-xs-12 col-sm-6">
                                <div class="feature-box"><span class="fa fa-share"></span>
                                    <div class="content">
                                        <h3><?php echo wp_specialchars_decode($linea_Options['header_returnservice_banner']); ?> </h3>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php if (!empty($linea_Options['header_moneyback_banner'])) : ?>
                            <div class="col-lg-3 col-xs-12 col-sm-6">
                                <div class="feature-box last"><span class="fa fa-phone"></span>
                                    <div class="content">
                                        <h3><?php echo wp_specialchars_decode($linea_Options['header_moneyback_banner']); ?></h3>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>

                    </div>
                </div>
            </div>


        <?php

        endif;
    }
}


if (!function_exists('linea_footer_service')) {
    function linea_footer_service()
    {

        global $linea_Options;

        if (isset($linea_Options['header_show_info_banner']) && !empty($linea_Options['header_show_info_banner'])) :
            ?>


            <div class="our-features-box hidden-xs">
                <div class="features-block">

                    <div class="col-lg-7 col-md-9 col-xs-12 col-sm-9 offer-block">

                        <?php if (!empty($linea_Options['shipping_banner'])) : ?>
                            <div class="feature-box first">
                                <div class="content">
                                    <h3><?php echo wp_specialchars_decode($linea_Options['shipping_banner']); ?> </h3>
                                </div>
                            </div>
                        <?php endif; ?>

                        <span class="separator">/</span>

                        <?php if (!empty($linea_Options['header_customer_support_banner'])) : ?>
                            <div class="feature-box">
                                <div class="content">
                                    <h3><?php echo wp_specialchars_decode($linea_Options['header_customer_support_banner']); ?></h3>
                                </div>
                            </div>
                        <?php endif; ?>

                        <span class="separator">/</span>

                        <?php if (!empty($linea_Options['header_moneyback_banner'])) : ?>
                            <div class="feature-box last">
                                <div class="content">
                                    <h3><?php echo wp_specialchars_decode($linea_Options['header_moneyback_banner']); ?></h3>
                                </div>
                            </div>
                        <?php endif; ?>

                    </div>

                    <?php if (!empty($linea_Options['header_returnservice_banner'])) : ?>
                        <div class="col-lg-2 col-md-1 col-sm-2 hidden-sm hidden-md"><span
                                    class="offer-label"><?php echo wp_specialchars_decode($linea_Options['header_returnservice_banner']); ?></span>
                        </div>
                    <?php endif; ?>

                </div>
            </div>

        <?php

        endif;
    }
}


if (!function_exists('linea_home_page_promotion_banners')) {
    function linea_home_page_promotion_banners()
    {
        global $linea_Options;
        ?>
        <?php
        if (isset($linea_Options['enable_home_page_promotion_banners']) && $linea_Options['enable_home_page_promotion_banners']) {
            ?>

            <div class="promotion-banner">
                <div class="row">

                    <?php if (isset($linea_Options['home-banner1']) && !empty($linea_Options['home-banner1']['url'])) { ?>

                        <div class="col-lg-5 col-sm-5">
                            <a href="<?php echo !empty($linea_Options['home-banner1-url']) ? esc_url($linea_Options['home-banner1-url']) : '#' ?>"
                               title="<?php echo esc_html($linea_Options['home-banner1-title']); ?>">
                                <?php if (isset($linea_Options['home-banner1-text']) && !empty($linea_Options['home-banner1-text'])) { ?>
                                    <?php echo wp_specialchars_decode($linea_Options['home-banner1-text']); ?>
                                <?php } ?>
                                <img src="<?php echo esc_url($linea_Options['home-banner1']['url']); ?>"
                                     alt="<?php echo esc_html($linea_Options['home-banner1-title']); ?>">

                            </a>
                        </div>

                    <?php } ?>

                    <?php if (isset($linea_Options['home-banner2']) && !empty($linea_Options['home-banner2']['url'])) { ?>

                        <div class="col-lg-7 col-sm-7">
                            <a href="<?php echo !empty($linea_Options['home-banner2-url']) ? esc_url($linea_Options['home-banner2-url']) : '#' ?>"
                               title="<?php echo esc_html($linea_Options['home-banner2-title']); ?>">
                                <?php if (isset($linea_Options['home-banner2-text']) && !empty($linea_Options['home-banner2-text'])) { ?>
                                    <?php echo wp_specialchars_decode($linea_Options['home-banner2-text']); ?>
                                <?php } ?>
                                <img src="<?php echo esc_url($linea_Options['home-banner2']['url']); ?>"
                                     alt="<?php echo esc_html($linea_Options['home-banner2-title']); ?>">

                            </a>
                        </div>

                    <?php } ?>
                </div>
            </div>
        <?php } ?>


        <?php

    }
}


if (!function_exists('linea_hotdeal_product')) {
    function linea_hotdeal_product()
    {
        global $linea_Options;
        if (isset($linea_Options['enable_home_hotdeal_products']) && !empty($linea_Options['enable_home_hotdeal_products'])) {

            $product_ids_on_sale = wc_get_featured_product_ids();
            ?>
            <div class="hot-deal">
                <ul class="products-grid">
                    <?php
                    $today = date('Y-m-d');

                    $args = array(
                        'post_type' => 'product',
                        'post_status' => 'publish',
                        'posts_per_page' => 1,
                        'meta_key' => 'hotdeal_on_home',
                        'meta_value' => 'yes',
                        'post__in' => $product_ids_on_sale,
                        'meta_query' => WC()->query->get_meta_query(),

                        'order' => 'asc'


                    );

                    $loop = new WP_Query($args);

                    if ($loop->have_posts()) {
                        while ($loop->have_posts()) : $loop->the_post();
                            linea_hotdeal_template();
                        endwhile;
                    } else {
                        esc_attr_e('No products found', 'linea');
                    }
                    wp_reset_postdata();
                    ?>

                </ul>
            </div>

            <?php
        }
    }
}


if (!function_exists('linea_new_products')) {
    function linea_new_products()
    {
        global $linea_Options;

        if (isset($linea_Options['enable_home_new_products']) && !empty($linea_Options['enable_home_new_products']) && !empty($linea_Options['home_new_products_categories'])) { ?>

            <div class="category-product">
                <div class="navbar nav-menu">
                    <div class="navbar-collapse">
                        <div class="new_title">

                            <?php if (isset($linea_Options['new_products_title']) && !empty($linea_Options['new_products_title'])) { ?>
                                <h2><?php echo wp_specialchars_decode($linea_Options['new_products_title']); ?></h2>
                                <?php
                            } else {
                                ?>
                                <h2><?php esc_attr_e('New Products', 'linea'); ?></h2>
                                <?php
                            } ?>

                        </div>


                        <ul class="nav navbar-nav">
                            <?php
                            $catloop = 1;

                            foreach ($linea_Options['home_new_products_categories'] as $category) {
                                $term = get_term_by('id', $category, 'product_cat', 'ARRAY_A');

                                ?>
                                <li class="<?php if ($catloop == 1) { ?> active <?php } ?>">
                                    <a href="#cat-<?php echo esc_html($category) ?>"
                                       data-toggle="tab"><?php echo esc_html($term['name']); ?>
                                    </a>
                                </li>

                                <?php
                                $catloop++;
                            } ?>
                        </ul>
                    </div>
                </div>

                <!-- Tab panes -->
                <div class="product-bestseller">
                    <div class="product-bestseller-content">
                        <div class="product-bestseller-list">
                            <div class="tab-container">
                                <?php
                                $contentloop = 1;
                                foreach ($linea_Options['home_new_products_categories'] as $catcontent) {
                                    $term = get_term_by('id', $catcontent, 'product_cat', 'ARRAY_A');
                                    ?>
                                    <div class="tab-panel <?php if ($contentloop == 1) { ?> active <?php } ?>"
                                         id="cat-<?php echo esc_html($catcontent); ?>">
                                        <div class="category-products">
                                            <ul class="products-grid">

                                                <?php

                                                $args = array(
                                                    'post_type' => 'product',
                                                    'post_status' => 'publish',
                                                    'ignore_sticky_posts' => 1,
                                                    'posts_per_page' => $linea_Options['new_products_per_page'],

                                                    'orderby' => 'date',
                                                    'order' => 'DESC',
                                                    'tax_query' => array(

                                                        array(
                                                            'taxonomy' => 'product_cat',
                                                            'field' => 'term_id',
                                                            'terms' => $catcontent
                                                        )
                                                    ),


                                                );

                                                $loop = new WP_Query($args);

                                                if ($loop->have_posts()) {
                                                    while ($loop->have_posts()) : $loop->the_post();
                                                        linea_new_products_template();
                                                    endwhile;
                                                } else {
                                                    esc_attr_e('No products found', 'linea');
                                                }

                                                wp_reset_postdata();
                                                $contentloop++;

                                                ?>

                                            </ul>
                                        </div>
                                    </div>
                                <?php } ?>

                            </div>
                        </div>
                    </div>
                </div>

            </div>


            <?php
        }
    }
}

if (!function_exists('linea_featured_products')) {
    function linea_featured_products()
    {

        global $linea_Options;
        if (isset($linea_Options['enable_home_featured_products']) && !empty($linea_Options['enable_home_featured_products'])) {
            ?>

            <div class="featured-pro-block">
                <div class="slider-items-products">

                    <div class="new-arrivals-block">
                        <div class="block-title">
                            <h2><?php esc_attr_e('Featured Product', 'linea'); ?></h2>
                        </div>
                    </div>

                    <div id="new-arrivals-slider" class="product-flexslider hidden-buttons">
                        <div class="home-block-inner"></div>
                        <div class="slider-items slider-width-col4 products-grid block-content">
                            <?php
                            $args = array(
                                'post_type' => 'product',
                                'post_status' => 'publish',
                                'posts_per_page' => $linea_Options['featured_per_page'],
                                'tax_query' => array(
                                    array(
                                        'taxonomy' => 'product_visibility',
                                        'field' => 'name',
                                        'terms' => 'featured',
                                    ),
                                ),
                            );


                            $loop = new WP_Query($args);
                            if ($loop->have_posts()) {
                                while ($loop->have_posts()) : $loop->the_post();
                                    linea_products_template();
                                endwhile;
                            } else {
                                esc_attr_e('No products found', 'linea');
                            }

                            wp_reset_postdata();
                            ?>

                        </div>
                    </div>

                </div>
            </div>

            <?php
        }
    }
}

if (!function_exists('linea_bestseller_products')) {
    function linea_bestseller_products()
    {
        global $linea_Options;

        if (isset($linea_Options['enable_home_bestseller_products']) && !empty($linea_Options['enable_home_bestseller_products'])) {
            ?>


            <div class="bestsell-pro">
                <div>
                    <div class="slider-items-products">
                        <div class="bestsell-block">

                            <div class="block-title">
                                <h2><?php esc_attr_e('Best Sellers ', 'linea'); ?></h2>
                            </div>

                            <div id="bestsell-slider" class="product-flexslider hidden-buttons">
                                <div class="slider-items slider-width-col4 products-grid block-content">


                                    <?php

                                    $args = array(
                                        'post_type' => 'product',
                                        'post_status' => 'publish',
                                        'ignore_sticky_posts' => 1,
                                        'posts_per_page' => $linea_Options['bestseller_per_page'],
                                        'meta_key' => 'total_sales',
                                        'orderby' => 'meta_value_num',
                                        'order' => 'DESC'
                                    );

                                    $loop = new WP_Query($args);

                                    if ($loop->have_posts()) {
                                        while ($loop->have_posts()) : $loop->the_post();
                                            linea_bestseller_products_template();

                                        endwhile;
                                    } else {
                                        esc_attr_e('No products found', 'linea');
                                    }

                                    wp_reset_postdata();


                                    ?>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>


        <?php } ?>
        <?php

    }
}

if (!function_exists('linea_bestseller_products_template')) {
    function linea_bestseller_products_template()
    {

        $Linea = new Linea();
        global $product, $yith_wcwl, $post;
        $imageUrl = wc_placeholder_img_src();

        if (has_post_thumbnail())
            $imageUrl = wp_get_attachment_image_src(get_post_thumbnail_id(), 'linea-product-size-large1');

        ?>
        <div class="item">
            <div class="item-inner">
                <div class="item-img">
                    <div class="item-img-info">
                        <a href="<?php the_permalink(); ?>" title="<?php echo esc_html($post->post_title); ?>"
                           class="product-image">
                            <figure class="img-responsive">
                                <img alt="<?php echo esc_html($post->post_title); ?>"
                                     src="<?php echo esc_url($imageUrl[0]); ?>">
                            </figure>
                        </a>
                        <?php if ($product->is_on_sale()) : ?>
                            <div class="sale-label sale-top-right">
                                <?php esc_attr_e('Sale', 'linea'); ?>
                            </div>
                        <?php endif; ?>

                        <?php if (!empty($hotdeal) && !empty($sales_price_to)) {
                            ?>
                            <div class="box-timer">
                                <div class="countbox_1 timer-grid"
                                     data-time="<?php echo esc_html($sales_price_date_to); ?>">
                                </div>
                            </div>

                        <?php } ?>


                        <div class="box-hover">
                            <ul class="add-to-links">
                                <li>

                                    <?php if (class_exists('YITH_WCQV_Frontend')) { ?>

                                        <a class="detail-bnt yith-wcqv-button link-quickview"
                                           data-product_id="<?php echo esc_html($product->get_id()); ?>"></a>

                                    <?php } ?>

                                <li>
                                    <?php if (isset($yith_wcwl) && is_object($yith_wcwl)) {

                                        $classes = get_option('yith_wcwl_use_button') == 'yes' ? 'class="add_to_wishlist link-wishlist"' : 'class="add_to_wishlist link-wishlist"';
                                        ?>
                                        <a title="<?php esc_attr_e('Add to Wishlist', 'linea'); ?>"
                                           href="<?php echo esc_url(add_query_arg('add_to_wishlist', $product->get_id())) ?>"
                                           data-product-id="<?php echo esc_html($product->get_id()); ?>"
                                           data-product-type="<?php echo esc_html($product->get_type()); ?>" <?php echo wp_specialchars_decode($classes); ?>></a>

                                    <?php } ?>
                                </li>
                                <li>

                                    <?php if (class_exists('YITH_Woocompare_Frontend')) {
                                        $mgk_yith_cmp = new YITH_Woocompare_Frontend; ?>

                                        <a title="<?php esc_attr_e('Add to Compare', 'linea'); ?>"
                                           href="<?php echo esc_url($mgk_yith_cmp->add_product_url($product->get_id())); ?>"
                                           class="link-compare add_to_compare compare "
                                           data-product_id="<?php echo esc_html($product->get_id()); ?>"></a>

                                    <?php } ?>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="item-info">
                    <div class="info-inner">

                        <div class="item-title">
                            <a href="<?php the_permalink(); ?>"
                               title="<?php echo esc_html($post->post_title); ?>"> <?php echo esc_html($post->post_title); ?> </a>
                        </div>

                        <div class="item-content">

                            <div class="rating">
                                <div class="ratings">
                                    <div class="rating-box">
                                        <?php $average = $product->get_average_rating(); ?>
                                        <div style="width:<?php echo esc_html(($average / 5) * 100); ?>%"
                                             class="rating"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="item-price">
                                <div class="price-box">
                                    <?php echo wp_specialchars_decode($product->get_price_html()); ?>
                                </div>
                            </div>

                            <div class="action">
                                <?php
                                /**
                                 * woocommerce_after_shop_loop_item hook
                                 *
                                 * @hooked woocommerce_template_loop_add_to_cart - 10
                                 */
                                do_action('woocommerce_after_shop_loop_item');

                                ?>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>


        <?php

    }
}

if (!function_exists('linea_featured_products_template')) {
    function linea_featured_products_template()
    {

        $Linea = new Linea();
        global $product, $yith_wcwl, $post;
        $imageUrl = wc_placeholder_img_src();
        if (has_post_thumbnail())
            $imageUrl = wp_get_attachment_image_src(get_post_thumbnail_id(), 'linea-product-size-large');

        ?>

        <div class="item">
            <div class="item-inner">
                <div class="item-img">
                    <div class="item-img-info">
                        <a href="<?php the_permalink(); ?>" title="<?php echo esc_html($post->post_title); ?>"
                           class="product-image">
                            <figure class="img-responsive">
                                <img alt="<?php echo esc_html($post->post_title); ?>"
                                     src="<?php echo esc_url($imageUrl[0]); ?>">
                            </figure>
                        </a>


                        <?php if ($product->is_on_sale()) : ?>

                            <?php echo apply_filters('woocommerce_sale_flash', ' <div class="sale-label sale-top-left">' . esc_html__('Sale', 'woocommerce') . '</div>', $post, $product); ?>

                        <?php endif; ?>
                        <div class="item-box-hover">
                            <div class="box-inner">


                                <?php if (class_exists('YITH_WCQV_Frontend')) { ?>
                                    <div class="product-detail-bnt">
                                        <a class="button detail-bnt yith-wcqv-button link-quickview"
                                           data-product_id="<?php echo esc_html($product->get_id()); ?>"><span><?php esc_attr_e('Quick View', 'linea'); ?></span></a>
                                    </div>
                                <?php } ?>

                                <div class="actions"><span class="add-to-links">


                                  <?php if (isset($yith_wcwl) && is_object($yith_wcwl)) {

                                      $classes = get_option('yith_wcwl_use_button') == 'yes' ? 'class="add_to_wishlist link-wishlist"' : 'class="add_to_wishlist link-wishlist"';
                                      ?>
                                      <a title="<?php esc_attr_e('Add to Wishlist', 'linea'); ?>"
                                         href="<?php echo esc_url(add_query_arg('add_to_wishlist', $product->get_id())) ?>"
                                         data-product-id="<?php echo esc_html($product->get_id()); ?>"
                                         data-product-type="<?php echo esc_html($product->get_type()); ?>" <?php echo wp_specialchars_decode($classes); ?>><span><?php esc_attr_e('Add to Wishlist', 'linea'); ?></span></a>

                                  <?php } ?>


                                        <?php if (class_exists('YITH_Woocompare_Frontend')) {
                                            $mgk_yith_cmp = new YITH_Woocompare_Frontend; ?>

                                            <a title="<?php esc_attr_e('Add to Compare', 'linea'); ?>"
                                               href="<?php echo esc_url($mgk_yith_cmp->add_product_url($product->get_id())); ?>"
                                               class="compare link-compare add_to_compare"
                                               data-product_id="<?php echo esc_html($product->get_id()); ?>"></a>

                                        <?php } ?> 

                                 </span></div>

                                <div class="add_cart">
                                    <?php linea_woocommerce_product_add_to_cart_text(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item-info">
                    <div class="info-inner">
                        <div class="item-title">
                            <a href="<?php the_permalink(); ?>"
                               title="<?php echo esc_html($post->post_title); ?>"> <?php echo esc_html($post->post_title); ?> </a>
                        </div>
                        <div class="item-content">
                            <div class="rating">
                                <div class="ratings">
                                    <div class="rating-box">
                                        <?php $average = $product->get_average_rating(); ?>
                                        <div style="width:<?php echo esc_html(($average / 5) * 100); ?>%"
                                             class="rating"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="item-price">
                                <div class="price-box">
                                    <?php echo wp_specialchars_decode($product->get_price_html()); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <?php

    }
}


if (!function_exists('linea_new_products_template')) {
    function linea_new_products_template()
    {

        $Linea = new Linea();
        global $product, $yith_wcwl, $post, $linea_Options;
        $totalproduct = $linea_Options['new_products_per_page'];
        $imageUrl = wc_placeholder_img_src();
        if (has_post_thumbnail())
            $imageUrl = wp_get_attachment_image_src(get_post_thumbnail_id(), 'linea-product-size-large');
        ?>

        <li class="item col-lg-3 col-md-3 col-sm-4 col-xs-6">
            <div class="item-inner">
                <div class="item-img">
                    <div class="item-img-info">
                        <a href="<?php the_permalink(); ?>" title="<?php echo esc_html($post->post_title); ?>"
                           class="product-image">
                            <figure class="img-responsive">
                                <img alt="<?php echo esc_html($post->post_title); ?>"
                                     src="<?php echo esc_url($imageUrl[0]); ?>">
                            </figure>
                        </a>
                        <?php if ($product->is_on_sale()) : ?>
                            <div class="sale-label sale-top-right">
                                <?php esc_attr_e('Sale', 'linea'); ?>
                            </div>
                        <?php endif; ?>
                        <div class="box-hover">
                            <ul class="add-to-links">
                                <li>

                                    <?php if (class_exists('YITH_WCQV_Frontend')) { ?>

                                        <a class="detail-bnt yith-wcqv-button link-quickview"
                                           data-product_id="<?php echo esc_html($product->get_id()); ?>"></a>

                                    <?php } ?>

                                <li>
                                    <?php if (isset($yith_wcwl) && is_object($yith_wcwl)) {

                                        $classes = get_option('yith_wcwl_use_button') == 'yes' ? 'class="add_to_wishlist link-wishlist"' : 'class="add_to_wishlist link-wishlist"';
                                        ?>
                                        <a title="<?php esc_attr_e('Add to Wishlist', 'linea'); ?>"
                                           href="<?php echo esc_url(add_query_arg('add_to_wishlist', $product->get_id())) ?>"
                                           data-product-id="<?php echo esc_html($product->get_id()); ?>"
                                           data-product-type="<?php echo esc_html($product->get_type()); ?>" <?php echo wp_specialchars_decode($classes); ?>></a>

                                    <?php } ?>
                                </li>
                                <li>

                                    <?php if (class_exists('YITH_Woocompare_Frontend')) {
                                        $mgk_yith_cmp = new YITH_Woocompare_Frontend; ?>

                                        <a title="<?php esc_attr_e('Add to Compare', 'linea'); ?>"
                                           href="<?php echo esc_url($mgk_yith_cmp->add_product_url($product->get_id())); ?>"
                                           class="link-compare add_to_compare compare "
                                           data-product_id="<?php echo esc_html($product->get_id()); ?>"></a>

                                    <?php } ?>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="item-info">
                    <div class="info-inner">

                        <div class="item-title">
                            <a href="<?php the_permalink(); ?>"
                               title="<?php echo esc_html($post->post_title); ?>"> <?php echo esc_html($post->post_title); ?> </a>
                        </div>

                        <div class="item-content">

                            <div class="rating">
                                <div class="ratings">
                                    <div class="rating-box">
                                        <?php $average = $product->get_average_rating(); ?>
                                        <div style="width:<?php echo esc_html(($average / 5) * 100); ?>%"
                                             class="rating"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="item-price">
                                <div class="price-box">
                                    <?php echo wp_specialchars_decode($product->get_price_html()); ?>
                                </div>
                            </div>

                            <div class="action">
                                <?php
                                /**
                                 * woocommerce_after_shop_loop_item hook
                                 *
                                 * @hooked woocommerce_template_loop_add_to_cart - 10
                                 */
                                do_action('woocommerce_after_shop_loop_item');

                                ?>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </li>


        <?php

    }
}


if (!function_exists('linea_products_template')) {
    function linea_products_template()
    {

        $Linea = new Linea();
        global $product, $yith_wcwl, $post;
        $imageUrl = wc_placeholder_img_src();
        if (has_post_thumbnail())
            $imageUrl = wp_get_attachment_image_src(get_post_thumbnail_id(), 'linea-product-size-large');

        ?>
        <div class="item">
            <div class="item-inner">
                <div class="item-img">
                    <div class="item-img-info">
                        <a href="<?php the_permalink(); ?>" title="<?php echo esc_html($post->post_title); ?>"
                           class="product-image">
                            <figure class="img-responsive">
                                <img alt="<?php echo esc_html($post->post_title); ?>"
                                     src="<?php echo esc_url($imageUrl[0]); ?>">
                            </figure>
                        </a>
                        <?php if ($product->is_on_sale()) : ?>
                            <div class="sale-label sale-top-right">
                                <?php esc_attr_e('Sale', 'linea'); ?>
                            </div>
                        <?php endif; ?>
                        <div class="box-hover">
                            <ul class="add-to-links">
                                <li>

                                    <?php if (class_exists('YITH_WCQV_Frontend')) { ?>

                                        <a class="detail-bnt yith-wcqv-button link-quickview"
                                           data-product_id="<?php echo esc_html($product->get_id()); ?>"></a>

                                    <?php } ?>

                                <li>
                                    <?php if (isset($yith_wcwl) && is_object($yith_wcwl)) {

                                        $classes = get_option('yith_wcwl_use_button') == 'yes' ? 'class="add_to_wishlist link-wishlist"' : 'class="add_to_wishlist link-wishlist"';
                                        ?>
                                        <a title="<?php esc_attr_e('Add to Wishlist', 'linea'); ?>"
                                           href="<?php echo esc_url(add_query_arg('add_to_wishlist', $product->get_id())) ?>"
                                           data-product-id="<?php echo esc_html($product->get_id()); ?>"
                                           data-product-type="<?php echo esc_html($product->get_type()); ?>" <?php echo wp_specialchars_decode($classes); ?>></a>

                                    <?php } ?>
                                </li>
                                <li>

                                    <?php if (class_exists('YITH_Woocompare_Frontend')) {
                                        $mgk_yith_cmp = new YITH_Woocompare_Frontend; ?>

                                        <a title="<?php esc_attr_e('Add to Compare', 'linea'); ?>"
                                           href="<?php echo esc_url($mgk_yith_cmp->add_product_url($product->get_id())); ?>"
                                           class="link-compare add_to_compare compare "
                                           data-product_id="<?php echo esc_html($product->get_id()); ?>"></a>

                                    <?php } ?>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="item-info">
                    <div class="info-inner">

                        <div class="item-title">
                            <a href="<?php the_permalink(); ?>"
                               title="<?php echo esc_html($post->post_title); ?>"> <?php echo esc_html($post->post_title); ?> </a>
                        </div>

                        <div class="item-content">

                            <div class="rating">
                                <div class="ratings">
                                    <div class="rating-box">
                                        <?php $average = $product->get_average_rating(); ?>
                                        <div style="width:<?php echo esc_html(($average / 5) * 100); ?>%"
                                             class="rating"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="item-price">
                                <div class="price-box">
                                    <?php echo wp_specialchars_decode($product->get_price_html()); ?>
                                </div>
                            </div>

                            <div class="action">
                                <?php
                                /**
                                 * woocommerce_after_shop_loop_item hook
                                 *
                                 * @hooked woocommerce_template_loop_add_to_cart - 10
                                 */
                                do_action('woocommerce_after_shop_loop_item');

                                ?>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>


        <?php

    }
}


if (!function_exists('linea_related_upsell_template')) {
    function linea_related_upsell_template()
    {
        $Linea = new Linea();
        global $product, $yith_wcwl, $post;


        $imageUrl = wc_placeholder_img_src();
        if (has_post_thumbnail())
            $imageUrl = wp_get_attachment_image_src(get_post_thumbnail_id(), 'linea-product-size-large');
        ?>

        <div class="item">
            <div class="item-inner">
                <div class="item-img">
                    <div class="item-img-info">
                        <a href="<?php the_permalink(); ?>" title="<?php echo esc_html($post->post_title); ?>"
                           class="product-image">
                            <figure class="img-responsive">
                                <img alt="<?php echo esc_html($post->post_title); ?>"
                                     src="<?php echo esc_url($imageUrl[0]); ?>">
                            </figure>
                        </a>
                        <?php if ($product->is_on_sale()) : ?>
                            <div class="sale-label sale-top-right">
                                <?php esc_attr_e('Sale', 'linea'); ?>
                            </div>
                        <?php endif; ?>
                        <div class="box-hover">
                            <ul class="add-to-links">
                                <li>

                                    <?php if (class_exists('YITH_WCQV_Frontend')) { ?>

                                        <a class="detail-bnt yith-wcqv-button link-quickview"
                                           data-product_id="<?php echo esc_html($product->get_id()); ?>"></a>

                                    <?php } ?>

                                <li>
                                    <?php if (isset($yith_wcwl) && is_object($yith_wcwl)) {

                                        $classes = get_option('yith_wcwl_use_button') == 'yes' ? 'class="add_to_wishlist link-wishlist"' : 'class="add_to_wishlist link-wishlist"';
                                        ?>
                                        <a title="<?php esc_attr_e('Add to Wishlist', 'linea'); ?>"
                                           href="<?php echo esc_url(add_query_arg('add_to_wishlist', $product->get_id())) ?>"
                                           data-product-id="<?php echo esc_html($product->get_id()); ?>"
                                           data-product-type="<?php echo esc_html($product->get_type()); ?>" <?php echo wp_specialchars_decode($classes); ?>></a>

                                    <?php } ?>
                                </li>
                                <li>

                                    <?php if (class_exists('YITH_Woocompare_Frontend')) {
                                        $mgk_yith_cmp = new YITH_Woocompare_Frontend; ?>

                                        <a title="<?php esc_attr_e('Add to Compare', 'linea'); ?>"
                                           href="<?php echo esc_url($mgk_yith_cmp->add_product_url($product->get_id())); ?>"
                                           class="link-compare add_to_compare compare "
                                           data-product_id="<?php echo esc_html($product->get_id()); ?>"></a>

                                    <?php } ?>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="item-info">
                    <div class="info-inner">

                        <div class="item-title">
                            <a href="<?php the_permalink(); ?>"
                               title="<?php echo esc_html($post->post_title); ?>"> <?php echo esc_html($post->post_title); ?> </a>
                        </div>

                        <div class="item-content">

                            <div class="rating">
                                <div class="ratings">
                                    <div class="rating-box">
                                        <?php $average = $product->get_average_rating(); ?>
                                        <div style="width:<?php echo esc_html(($average / 5) * 100); ?>%"
                                             class="rating"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="item-price">
                                <div class="price-box">
                                    <?php echo wp_specialchars_decode($product->get_price_html()); ?>
                                </div>
                            </div>

                            <div class="action">
                                <?php
                                /**
                                 * woocommerce_after_shop_loop_item hook
                                 *
                                 * @hooked woocommerce_template_loop_add_to_cart - 10
                                 */
                                do_action('woocommerce_after_shop_loop_item');

                                ?>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php
    }
}

if (!function_exists('linea_home_custom_slider_wrap')) {
    function linea_home_custom_slider_wrap()
    {
        global $linea_Options;

        if (isset($linea_Options['enable_home_page_custom_slider']) && $linea_Options['enable_home_page_custom_slider'] && isset($linea_Options['enable_home_page_custom_slider']) && !empty($linea_Options['enable_home_page_custom_slider'])) {

            ?>
            <div class="custom-slider-wrap">
                <div class="custom-slider-inner">
                    <div class="home-custom-slider">
                        <div>
                            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                <ol class="carousel-indicators">
                                    <?php $j = 0; ?>
                                    <?php foreach ($linea_Options['home_page_bottom_slider'] as $slide) : ?>
                                        <li class="<?php if ($j == 0) { ?> active <?php } ?>"
                                            data-target="#carousel-example-generic"
                                            data-slide-to="<?php echo esc_html($j); ?>"></li>
                                        <?php
                                        $j++;
                                    endforeach; ?>
                                </ol>

                                <div class="carousel-inner">

                                    <?php
                                    $i = 0;
                                    foreach ($linea_Options['home_page_bottom_slider'] as $slide) : ?>

                                        <div class="item <?php if ($i == 0) { ?> active <?php } ?>">
                                            <img src="<?php echo esc_url($slide['image']); ?>"
                                                 alt="<?php echo esc_attr($slide['title']); ?>"/>

                                            <div class="carousel-caption">
                                                <?php echo wp_specialchars_decode($slide['description']); ?>
                                            </div>


                                        </div>

                                        <?php
                                        $i++;
                                    endforeach; ?>
                                </div>

                                <a class="left carousel-control" href="#" data-slide="prev"> <span
                                            class="sr-only"><?php esc_attr_e('Previous', 'linea'); ?></span> </a> <a
                                        class="right carousel-control" href="#" data-slide="next"> <span
                                            class="sr-only"><?php esc_attr_e('Next', 'linea'); ?></span> </a>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        <?php } ?>

        <?php

    }
}

if (!function_exists('linea_hotdeal_template')) {
    function linea_hotdeal_template()
    {
        $Linea = new Linea();
        global $product, $yith_wcwl, $post, $linea_Options;
        $imageUrl = wc_placeholder_img_src();
        if (has_post_thumbnail())
            $imageUrl = wp_get_attachment_image_src(get_post_thumbnail_id(), 'linea-product-size-large1');

        $product_type = $product->get_type();

        if ($product_type == 'variable') {
            $available_variations = $product->get_available_variations();
            $variation_id = $available_variations[0]['variation_id'];
            $newid = $variation_id;
        } else {
            $newid = $post->ID;

        }
        $sales_price_to = get_post_meta($newid, '_sale_price_dates_to', true);
        if (!empty($sales_price_to)) {
            $sales_price_date_to = date("Y/m/d", $sales_price_to);
        } else {
            $sales_price_date_to = '';
        }
        $curdate = date("m/d/y h:i:s A");

        ?>


        <li class="right-space two-height item">
            <div class="item-inner">
                <div class="item-img">
                    <div class="item-img-info">
                        <a href="<?php the_permalink(); ?>" title="<?php echo esc_html($post->post_title); ?>"
                           class="product-image">
                            <figure class="img-responsive">
                                <?php echo get_the_post_thumbnail($post->id, array(307, 261)); ?>
                            </figure>
                        </a>
                        <?php if ($product->is_on_sale()) : ?>
                            <div class="sale-label sale-top-right">
                                <?php esc_attr_e('Sale', 'linea'); ?>
                            </div>
                        <?php endif; ?>
                        <div class="box-hover">
                            <ul class="add-to-links">
                                <li>

                                    <?php if (class_exists('YITH_WCQV_Frontend')) { ?>

                                        <a class="detail-bnt yith-wcqv-button link-quickview"
                                           data-product_id="<?php echo esc_html($product->get_id()); ?>"></a>

                                    <?php } ?>

                                <li>
                                    <?php if (isset($yith_wcwl) && is_object($yith_wcwl)) {

                                        $classes = get_option('yith_wcwl_use_button') == 'yes' ? 'class="add_to_wishlist link-wishlist"' : 'class="add_to_wishlist link-wishlist"';
                                        ?>
                                        <a title="<?php esc_attr_e('Add to Wishlist', 'linea'); ?>"
                                           href="<?php echo esc_url(add_query_arg('add_to_wishlist', $product->get_id())) ?>"
                                           data-product-id="<?php echo esc_html($product->get_id()); ?>"
                                           data-product-type="<?php echo esc_html($product->get_type()); ?>" <?php echo wp_specialchars_decode($classes); ?>></a>

                                    <?php } ?>
                                </li>
                                <li>

                                    <?php if (class_exists('YITH_Woocompare_Frontend')) {
                                        $mgk_yith_cmp = new YITH_Woocompare_Frontend; ?>

                                        <a title="<?php esc_attr_e('Add to Compare', 'linea'); ?>"
                                           href="<?php echo esc_url($mgk_yith_cmp->add_product_url($product->get_id())); ?>"
                                           class="link-compare add_to_compare compare "
                                           data-product_id="<?php echo esc_html($product->get_id()); ?>"></a>

                                    <?php } ?>
                                </li>
                            </ul>
                        </div>
                        <div class="box-timer">
                            <div class="countbox_1 timer-grid"
                                 data-time="<?php echo esc_html($sales_price_date_to); ?>">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="item-info">
                    <div class="info-inner">

                        <div class="item-title">
                            <a href="<?php the_permalink(); ?>"
                               title="<?php echo esc_html($post->post_title); ?>"> <?php echo esc_html($post->post_title); ?> </a>
                        </div>

                        <div class="item-content">

                            <div class="rating">
                                <div class="ratings">
                                    <div class="rating-box">
                                        <?php $average = $product->get_average_rating(); ?>
                                        <div style="width:<?php echo esc_html(($average / 5) * 100); ?>%"
                                             class="rating"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="item-price">
                                <div class="price-box">
                                    <?php echo wp_specialchars_decode($product->get_price_html()); ?>
                                </div>
                            </div>

                            <div class="action">
                                <?php
                                /**
                                 * woocommerce_after_shop_loop_item hook
                                 *
                                 * @hooked woocommerce_template_loop_add_to_cart - 10
                                 */
                                do_action('woocommerce_after_shop_loop_item');

                                ?>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </li>

        <?php

    }
}

if (!function_exists('linea_testimonials')) {
    function linea_testimonials()
    {
        global $linea_Options;
        ?>

        <?php if (isset($linea_Options['enable_testimonial']) && !empty($linea_Options['enable_testimonial']) && isset($linea_Options['all_testimonial']) && !empty($linea_Options['all_testimonial'])) { ?>

        <div class="testimonials">
            <div class="ts-testimonial-widget">
                <div class="slider-items-products">
                    <div id="testimonials-slider" class="product-flexslider hidden-buttons home-testimonials">
                        <div class="slider-items slider-width-col4 fadeInUp">

                            <?php foreach ($linea_Options['all_testimonial'] as $testimono) : ?>

                                <div class="holder">
                                    <div class="thumb">
                                        <img src="<?php echo esc_url($testimono['image']); ?>"
                                             data-bgposition='left top' data-bgfit='cover' data-bgrepeat='no-repeat'
                                             alt="<?php echo esc_html($testimono['title']); ?>"/>
                                    </div>
                                    <p><?php echo wp_specialchars_decode($testimono['description']); ?></p>

                                    <div class="line"></div>

                                    <strong class="name">
                                        <a href="<?php echo !empty($testimono['url']) ? esc_url($testimono['url']) : '#' ?>"
                                           target="_blank">
                                            <?php echo wp_specialchars_decode($testimono['title']); ?></a>
                                    </strong>
                                </div>

                            <?php endforeach; ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    <?php } ?>
        <?php

    }
}


if (!function_exists('linea_home_blog_posts')) {
    function linea_home_blog_posts()
    {
        $count = 0;
        global $linea_Options;
        $Linea = new Linea();
        if (isset($linea_Options['enable_home_blog_posts']) && !empty($linea_Options['enable_home_blog_posts'])) {
            ?>

            <div class="container">
                <div class="row">
                    <div class="blog-outer-container">

                        <div class="block-title">
                            <h2><?php esc_attr_e('Latest Blog', 'linea'); ?></h2>
                        </div>
                        <div class="blog-inner">
                            <?php

                            $args = array('posts_per_page' => 2, 'post__not_in' => get_option('sticky_posts'));
                            $the_query = new WP_Query($args);
                            $i = 1;
                            if ($the_query->have_posts()) :
                                while ($the_query->have_posts()) : $the_query->the_post(); ?>

                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                        <div class="entry-thumb image-hover2">
                                            <a href="<?php the_permalink(); ?>">
                                                <?php $image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'single-post-thumbnail'); ?>
                                                <img src="<?php echo esc_url($image[0]); ?>"
                                                     alt="<?php the_title_attribute(); ?>">
                                            </a>
                                        </div>

                                        <div class="blog-preview_info">

                                            <h4 class="blog-preview_title">
                                                <a href="<?php the_permalink(); ?>"><?php esc_html(the_title()); ?></a>
                                            </h4>

                                            <ul class="post-meta">
                                                <li>
                                                    <i class="fa fa-user"></i><?php esc_attr_e('posted by', 'linea'); ?>
                                                    <a href="<?php comments_link(); ?>"><?php the_author(); ?></a>
                                                </li>

                                                <li>
                                                    <i class="fa fa-comments"></i><a
                                                            href="<?php comments_link(); ?>"><?php comments_number('0', '1 ', '% '); ?><?php esc_attr_e('Comments', 'linea'); ?></a>
                                                </li>

                                                <li>
                                                    <i class="fa fa-clock-o"></i>
                                                    <time datetime="2015-10-04T07:17:52+00:00"
                                                          class="entry-date published"><?php esc_html(the_time('F d, Y')); ?></time>
                                                </li>
                                            </ul>


                                            <div class="blog-preview_desc">
                                                <?php the_excerpt(); ?>
                                            </div>
                                            <a class="blog-preview_btn"
                                               href="<?php the_permalink(); ?>"><?php esc_attr_e('READ MORE', 'linea'); ?></a>
                                        </div>
                                    </div>


                                    <?php $i++;
                                endwhile; ?>
                                <?php wp_reset_postdata(); ?>
                            <?php else: ?>
                                <p>
                                    <?php esc_attr_e('Sorry, no posts matched your criteria.', 'linea'); ?>
                                </p>
                            <?php endif;
                            ?>
                        </div>
                    </div>
                </div>
            </div>


            <?php
        }
    }
}


// new ccustom code section added

if (!function_exists('linea_home_customsection')) {
    function linea_home_customsection()
    {
        global $linea_Options;
        ?>

        <div class="container">
            <div class="row">
                <div class="mgk_custom_block">
                    <div class="col-md-12">
                        <?php the_content(); ?>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }

}

