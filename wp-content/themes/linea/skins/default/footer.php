<?php 

$Linea = new Linea();?>

 <footer class="footer">
    <?php linea_footer_signupform();?>
    <!--newsletter-->
    <div class="footer-middle">
      <div class="container">
        <div class="row">
                    
              <?php if (is_active_sidebar('footer-sidebar-1')) : ?>   
               <div class="col-md-3 col-sm-6">                          
                   <div class="footer-column pull-left">
                     <?php dynamic_sidebar('footer-sidebar-1'); ?>
                    </div>  
                </div>        
              <?php endif; ?>    
          
             <?php if (is_active_sidebar('footer-sidebar-2')) : ?>  
               <div class="col-md-3 col-sm-6">                         
                    <div class="footer-column pull-left">
                      <?php dynamic_sidebar('footer-sidebar-2'); ?>
                    </div>  
                </div>        
              <?php endif; ?>     

          
            <?php if (is_active_sidebar('footer-sidebar-3')) : ?>  
               <div class="col-md-3 col-sm-6">                         
                    <div class="footer-column pull-left">
                      <?php dynamic_sidebar('footer-sidebar-3'); ?>
                    </div>   
                </div>       
            <?php endif; ?>      

          
             <?php if (is_active_sidebar('footer-sidebar-4')) : ?>  
                <div class="col-md-3 col-sm-6">
                      <?php dynamic_sidebar('footer-sidebar-4'); ?> 
                 </div>                           
            <?php endif; ?> 
         

        </div>
      </div>
    </div>
    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-xs-12 col-sm-6">
            <?php linea_social_media_links(); ?>
          </div>

          <div class="col-xs-12 col-sm-6">
             <?php linea_footer_middle();?>            
          </div>
        </div>
      </div>
    </div>
  <?php linea_footer_text(); ?> 

 </footer>


</div>
<?php linea_backtotop();?>
    
<div class="menu-overlay"></div>
<?php // navigation panel
get_template_part('menu_panel');
 ?>
    <?php wp_footer(); ?>
</body></html>
