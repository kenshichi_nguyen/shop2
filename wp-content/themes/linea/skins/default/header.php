<!DOCTYPE html>
<html <?php language_attributes(); ?> id="parallax_scrolling">
<head>
<meta charset="<?php bloginfo('charset'); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
 <?php wp_head(); ?>
</head>
<?php
 $Linea = new Linea(); ?>
 
 
<body <?php body_class(); ?>>
   
<div id="page" class="page">

<header>

    <div class="header-container">
      <div class="container">
        <div class="row"> 
          <!-- Header Language -->
          <div class="col-xs-12 col-sm-4 col-md-6 col-lg-6 pull-left">
             <?php echo linea_currency_language(); ?>
            <div class="welcome-msg"> <?php linea_msg(); ?> </div>
          </div>
          <!-- Header Top Links -->
          <div class="col-xs-12 col-sm-8 col-md-6 col-lg-6 pull-right hidden-xs">
            <div class="toplinks">
              <div class="links">
                 <?php echo linea_top_navigation(); ?>
              </div>
            </div>
            <!-- End Header Top Links --> 
          </div>
        </div>
      </div>
    </div>

    <div class="container">
      <div class="row">

        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 logo-block"> 
          <!-- Header Logo -->
          <div class="logo"> 
              <?php linea_logo_image();?>
           </div>
          <!-- End Header Logo --> 
        </div>

        <div class="col-lg-7 col-md-6 col-sm-6 col-xs-3 hidden-xs category-search-form">
          <div class="search-box">
            <?php echo linea_search_form(); ?>            
          </div>
        </div>

        <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12 card_wishlist_area">

        <div class="mm-toggle-wrap">
          <div class="mm-toggle mobile-toggle"><i class="fa fa-align-justify"></i><span class="mm-label"><?php esc_attr_e('Menu', 'linea'); ?></span> </div>
        </div>
                 <?php
                    if (class_exists('WooCommerce')) :?>
                      <div class="top-cart-contain"> 
                       <?php linea_mini_cart(); ?>
                      </div>
                    <?php endif;
                  ?>

        </div>
      </div>
    </div>

<nav class="hidden-xs">
    <div class="nav-container">
           
           <?php if(has_nav_menu('main_menu'))
                  { ?> 
                  <div class="col-md-3 col-xs-12 col-sm-3">
                    <div class="mega-container visible-lg visible-md visible-sm">
                      <div class="navleft-container">
                        <div class="mega-menu-title">
                          <h3><i class="fa fa-navicon"></i><?php esc_attr_e('All Categories', 'linea'); ?> </h3>
                        </div>
                        <div class="mega-menu-category">
                            <?php echo linea_home_mobile_menu(); ?>                        
                        </div>
                      </div>
                     </div>
                  </div>

            <?php  }?>

        <?php linea_footer_service();?>
         
    </div>
</nav>
</header>


  <?php if (class_exists('WooCommerce') && is_woocommerce()) : ?>
    
      
  <?php endif; ?>
