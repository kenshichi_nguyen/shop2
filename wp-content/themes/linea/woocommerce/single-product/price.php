<?php
/**
 * Single Product Price, including microdata for SEO
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/price.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

?>
 <div class="price-box price"> <?php echo  wp_specialchars_decode($product->get_price_html()); ?> 
 <?php if($product->is_in_stock()){ ?><p class="availability in-stock pull-right"><span> <?php esc_attr_e('In Stock','linea');?></span></p> <?php  }else{ ?> <p class="availability out-of-stock pull-right"><span><?php esc_attr_e('Out of Stock','linea');?></span></p><?php }  ?>
 </div>
<div>
    <br>
    <h3>Liên hệ với nhân viên tư vấn</h3>
    <div id="button-contact-sigle-product" style="position: relative;display: block;height: 80px;">
        <!-- contact -->
        <!-- end contact -->
        <!-- viber -->
        <div id="viber-vr" class="button-contact-single">
            <div class="phone-vr">
                <div class="phone-vr-circle-fill"></div>
                <div class="phone-vr-img-circle">
                    <a target="_blank" href="viber://add?number=84586085555">
                        <img src="/wp-content/plugins/button-contact-vr/img/viber.png">
                    </a>
                </div>
            </div>
        </div>
        <!-- end viber -->

        <!-- zalo -->
        <div id="zalo-vr" class="button-contact-single">
            <div class="phone-vr">
                <div class="phone-vr-circle-fill"></div>
                <div class="phone-vr-img-circle">
                    <a target="_blank" href="https://zalo.me/84586085555">
                        <img src="/wp-content/plugins/button-contact-vr/img/zalo.png">
                    </a>
                </div>
            </div>
        </div>
        <!-- end zalo -->

        <!-- Phone -->
        <div id="phone-vr" class="button-contact-single">
            <div class="phone-vr">
                <div class="phone-vr-circle-fill"></div>
                <div class="phone-vr-img-circle">
                    <a href="tel:84586085555">
                        <img src="/wp-content/plugins/button-contact-vr/img/phone.png">
                    </a>
                </div>
            </div>
        </div>

        <!-- end phone -->

        <!-- Phone -->
        <div id="zalo-vr" class="button-contact-single">
            <div class="phone-vr">
                <div class="phone-vr-circle-fill"></div>
                <div class="phone-vr-img-circle">
                    <a target="_blank" href="http://m.me/TuiHieuLuxury">
                        <img src="/wp-content/plugins/button-contact-vr/img/facebook.png">
                    </a>
                </div>
            </div>
        </div>

        <!-- end phone -->
    </div>
</div>

