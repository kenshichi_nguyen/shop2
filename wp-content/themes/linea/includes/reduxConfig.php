<?php
    /**
     * ReduxFramework Sample Config File
     * For full documentation, please visit: http://docs.reduxframework.com/
     */

    if ( ! class_exists( 'Redux' ) ) {
        return;
    }


    // This is your option name where all the Redux data is stored.
    $opt_name = "mgk_option";

    // This line is only for altering the demo. Can be easily removed.
    $opt_name = apply_filters( 'redux_demo/opt_name', $opt_name );

    /*
     *
     * --> Used within different fields. Simply examples. Search for ACTUAL DECLARATION for field examples
     *
     */



    /**
     * ---> SET ARGUMENTS
     * All the possible arguments for Redux.
     * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
     * */

    $theme = wp_get_theme(); // For use with some settings. Not necessary.

    $args = array(
        // TYPICAL -> Change these values as you need/desire
        'opt_name'             => $opt_name,
        // This is where your data is stored in the database and also becomes your global variable name.
        'display_name'         => $theme->get( 'Name' ),
        // Name that appears at the top of your panel
        'display_version'      => $theme->get( 'Version' ),
        // Version that appears at the top of your panel
        'menu_type'            => 'menu',
        //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
        'allow_sub_menu'       => true,
        // Show the sections below the admin menu item or not
        'menu_title' => esc_html__('Linea Options', 'linea'),
        'page_title' => esc_html__('Linea Options', 'linea'),
        // You will need to generate a Google API key to use this feature.
        // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
        'google_api_key'       => '',
        // Set it you want google fonts to update weekly. A google_api_key value is required.
        'google_update_weekly' => false,
        // Must be defined to add google fonts to the typography module
        'async_typography'     => true,
        // Use a asynchronous font on the front end or font string
        //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
        'admin_bar'            => true,
        // Show the panel pages on the admin bar
        'admin_bar_icon'       => 'dashicons-portfolio',
        // Choose an icon for the admin bar menu
        'admin_bar_priority'   => 50,
        // Choose an priority for the admin bar menu
        'global_variable'      => 'linea_Options',
        // Set a different name for your global variable other than the opt_name
        'dev_mode'             => false,
        // Show the time the page took to load, etc
        'update_notice'        => true,
        // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
        'customizer'           => true,
       

        // OPTIONAL -> Give you extra features
        'page_priority'        => null,
        // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
        'page_parent'          => 'themes.php',
   
        'page_permissions'     => 'manage_options',
        // Permissions needed to access the options panel.
        'menu_icon'            => '',
        // Specify a custom URL to an icon
        'last_tab'             => '',
        // Force your panel to always open to a specific tab (by id)
        'page_icon'            => 'icon-themes',
        // Icon displayed in the admin panel next to your menu_title
        'page_slug'            => '',
        // Page slug used to denote the panel, will be based off page title then menu title then opt_name if not provided
        'save_defaults'        => true,
        // On load save the defaults to DB before user clicks save or not
        'default_show'         => false,
        // If true, shows the default value next to each field that is not the default value.
        'default_mark'         => '',
        // What to print by the field's title if the value shown is default. Suggested: *
        'show_import_export'   => true,
        // Shows the Import/Export panel when not used as a field.

        // CAREFUL -> These options are for advanced use only
        'transient_time'       => 60 * MINUTE_IN_SECONDS,
        'output'               => true,
        // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
        'output_tag'           => true,
        // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
                      // Disable the footer credit of Redux. Please leave if you can help it.

        // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
        'database'             => '',
        // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
        'use_cdn'              => true,
        // If you prefer not to use the CDN for Select2, Ace Editor, and others, you may download the Redux Vendor Support plugin yourself and run locally or embed it in your code.

        // HINTS
        'hints'                => array(
            'icon'          => 'el el-question-sign',
            'icon_position' => 'right',
            'icon_color'    => 'lightgray',
            'icon_size'     => 'normal',
            'tip_style'     => array(
                'color'   => 'red',
                'shadow'  => true,
                'rounded' => false,
                'style'   => '',
            ),
            'tip_position'  => array(
                'my' => 'top left',
                'at' => 'bottom right',
            ),
            'tip_effect'    => array(
                'show' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'mouseover',
                ),
                'hide' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'click mouseleave',
                ),
            ),
        )
    );

   



    // Panel Intro text -> before the form
    if ( ! isset( $args['global_variable'] ) || $args['global_variable'] !== false ) {
        if ( ! empty( $args['global_variable'] ) ) {
            $v = $args['global_variable'];
        } else {
            $v = str_replace( '-', '_', $args['opt_name'] );
        }
        $args['intro_text'] = sprintf(__( '<p>Did you know that Redux sets a global variable for you? To access any of your saved options from within your code you can use your global variable: <strong>$%1$s</strong></p>', 'linea' ), $v );
    } else {
        $args['intro_text'] = '<p>'.esc_html__('This text is displayed above the options panel. It isn\'t required, but more info is always better! The intro_text field accepts all HTML.', 'linea' ).'</p>';
    }
      // Add content after the form.
    $args['footer_text'] ='<p>'. esc_html__( 'This text is displayed below the options panel. It isn\'t required, but more info is always better! The footer_text field accepts all HTML.', 'linea' ).'</p>';


    Redux::setArgs( $opt_name, $args );

    /*
     * ---> END ARGUMENTS
     */


    /*
     * ---> START HELP TABS
     */

    $tabs = array(
        array(
            'id'      => 'redux-help-tab-1',
            'title'   => esc_html__( 'Theme Information 1', 'linea' ),
            'content' => esc_html__( 'This is the tab content, HTML is allowed.', 'linea' )
        ),
        array(
            'id'      => 'redux-help-tab-2',
            'title'   => esc_html__( 'Theme Information 2', 'linea' ),
            'content' => esc_html__( 'This is the tab content, HTML is allowed.', 'linea' )
        )
    );
    Redux::setHelpTab( $opt_name, $tabs );

    // Set the help sidebar
    $content = esc_html__( 'This is the sidebar content, HTML is allowed.', 'linea' );
    Redux::setHelpSidebar( $opt_name, $content );


    /*
     * <--- END HELP TABS
     */


    /*
     *
     * ---> START SECTIONS
     *
     */

    /*

        As of Redux 3.5+, there is an extensive API. This API can be used in a mix/match mode allowing for


     */
  global $woocommerce;
               $cat_arg=array();
               $cat_data='';
              
                if(class_exists('WooCommerce')) {
                   
                    $cat_data='terms';
                    $cat_arg=array('taxonomies'=>'product_cat', 'args'=>array());
                   }

    // -> START Basic Fields
    Redux::setSection( $opt_name, array(
                'title' => esc_html__('Home Settings', 'linea'),
                'desc' => esc_html__('Home page settings ', 'linea'),
                'icon' => 'el-icon-home',
             
                'fields' => array( 

                    array(
                   'id'       => 'theme_color',
                   'type'     => 'select',
                   'title'    => esc_html__('Select Color Variation', 'linea'), 
                    'desc'     => esc_html__('Select Color Variation', 'linea'),
                    // Must provide key => value pairs for select options
                    'options'  => array(
                     'emerald' => 'Emerald',
                     'blue' => 'Blue',
                     'yellow' => 'yellow',
                     'green' => 'Green'
                        ),
                     'default'  => 'emerald'
                     ),
                                
                    array(
                        'id' => 'enable_home_gallery',
                        'type' => 'switch',
                        'title' => esc_html__('Enable Home Page Gallery', 'linea'),
                        'subtitle' => esc_html__('You can enable/disable Home page Gallery', 'linea')
                    ),

                    array(
                        'id' => 'home-page-slider',
                        'type' => 'slides',
                        'title' => esc_html__('Home Slider Uploads', 'linea'),
                        'required' => array('enable_home_gallery', '=', '1'),
                        'subtitle' => esc_html__('Unlimited slide uploads with drag and drop sortings.', 'linea'),
                        'placeholder' => array(
                            'title' => esc_html__('This is a title', 'linea'),
                            'description' => esc_html__('Description Here', 'linea'),
                            'url' => esc_html__('Give us a link!', 'linea'),
                    ),

                    ),
                           
                    

                    array(
                        'id' => 'welcome_msg',
                        'type' => 'text',
                        'title' => esc_html__('Enter your welcome message here', 'linea'),
                       'subtitle' => esc_html__('Enter your welcome message here.', 'linea'),
                         'desc' => esc_html__('', 'linea'),      
                         ),
                    
                   array(
                        'id' => 'enable_home_side_banner',
                        'type' => 'switch',
                        'title' => esc_html__('Enable Home Page side Banner', 'linea'),
                        'subtitle' => esc_html__('You can enable/disable Home page Slider Bottom Banners', 'linea')
                    ),

                    array(
                        'id' => 'home-side-banner',
                        'type' => 'media',
                        'required' => array('enable_home_side_banner', '=', '1'),
                        'title' => esc_html__('Home side Banner 1', 'linea'),
                        'desc' => esc_html__('', 'linea'),
                        'subtitle' => esc_html__('Home side Banner 1', 'linea'),
                    ),

                    array(
                        'id' => 'home-side-banner-url',
                        'type' => 'text',
                        'required' => array('enable_home_side_banner', '=', '1'),
                        'title' => esc_html__('Home side Banner 1 URL', 'linea'),
                        'subtitle' => esc_html__('Home side Banner 1 URL', 'linea'),
                    ),
                     array(
                        'id' => 'home-side-banner_title',
                        'type' => 'text',
                        'required' => array('enable_home_side_banner', '=', '1'),
                        'title' => esc_html__('Home side banner image Title', 'linea'),
                        'subtitle' => esc_html__('Home side banner image Title', 'linea'),
                    ),

                     array(
                        'id' => 'home-side-banner-text',
                        'type' => 'text',
                        'required' => array('enable_home_side_banner', '=', '1'),
                        'title' => esc_html__('Home side Banner-1 text', 'linea'),
                        'subtitle' => esc_html__('Home side Banner-1 text', 'linea'),
                    ),


                    array(
                        'id' => 'background_image',
                        'type' => 'media',
                        'required' => array('enable_home_background_image', '=', '1'),
                        'title' => esc_html__('Theme Background Image', 'linea'),
                        'desc' => esc_html__('', 'linea'),
                        'subtitle' => esc_html__('Theme Background Image', 'linea'),
                    ),

                    array(
                        'id' => 'background_image-url',
                        'type' => 'text',
                        'required' => array('enable_home_background_image', '=', '1'),
                        'title' => esc_html__('Theme Background Image URL', 'linea'),
                        'subtitle' => esc_html__('Theme Background Image URL', 'linea'),
                    ),

                    array(
                        'id' => 'enable_home_page_promotion_banners',
                        'type' => 'switch',
                        'title' => esc_html__('Enable Home Page promotion Banners', 'linea'),
                        'subtitle' => esc_html__('You can enable/disable Home page promotion Banners', 'linea')
                    ),

                    array(
                        'id' => 'home-banner1',
                        'type' => 'media',
                        'required' => array('enable_home_page_promotion_banners', '=', '1'),
                        'title' => esc_html__('Home Banner 1', 'linea'),
                        'desc' => esc_html__('', 'linea'),
                        'subtitle' => esc_html__('Home Banner 1', 'linea'),
                    ),

                    array(
                        'id' => 'home-banner1-url',
                        'type' => 'text',
                        'required' => array('enable_home_page_promotion_banners', '=', '1'),
                        'title' => esc_html__('Home Banner 1 URL', 'linea'),
                        'subtitle' => esc_html__('Home Banner 1 URL', 'linea'),
                    ),

                    array(
                        'id' => 'home-banner1-title',
                        'type' => 'text',
                        'required' => array('enable_home_page_promotion_banners', '=', '1'),
                        'title' => esc_html__('Home Banner-1 Title', 'linea'),
                        'subtitle' => esc_html__('Home Banner-1 Title', 'linea'),
                    ),

                     array(
                        'id' => 'home-banner1-text',
                        'type' => 'text',
                        'required' => array('enable_home_page_promotion_banners', '=', '1'),
                        'title' => esc_html__('Home Banner-1 text', 'linea'),
                        'subtitle' => esc_html__('Home Banner-1 text', 'linea'),
                    ),

                    array(
                        'id' => 'home-banner2',
                        'type' => 'media',
                        'required' => array('enable_home_page_promotion_banners', '=', '1'),
                        'title' => esc_html__('Home Banner 2', 'linea'),
                        'desc' => esc_html__('', 'linea'),
                        'subtitle' => esc_html__('Home Banner 2', 'linea'),
                    ),

                    array(
                        'id' => 'home-banner2-url',
                        'type' => 'text',
                        'required' => array('enable_home_page_promotion_banners', '=', '1'),
                        'title' => esc_html__('Home Banner 2 URL', 'linea'),
                        'subtitle' => esc_html__('Home Banner 2 URL', 'linea'),
                    ),
                
                    array(
                        'id' => 'home-banner2-title',
                        'type' => 'text',
                        'required' => array('enable_home_page_promotion_banners', '=', '1'),
                        'title' => esc_html__('Home Banner-2 Title', 'linea'),
                        'subtitle' => esc_html__('Home Banner-2 Title', 'linea'),
                    ),

                     array(
                        'id' => 'home-banner2-text',
                        'type' => 'text',
                        'required' => array('enable_home_page_promotion_banners', '=', '1'),
                        'title' => esc_html__('Home Banner-2 text', 'linea'),
                        'subtitle' => esc_html__('Home Banner-2 text', 'linea'),
                    ),
                   
                   array(
                        'id' => 'enable_home_new_products',
                        'type' => 'switch',
                        'title' => esc_html__('Show New Products', 'linea'),
                        'subtitle' => esc_html__('You can show new products on home page.', 'linea')
                    ),

                    array(
                        'id' => 'new_products_title',
                        'type' => 'text',
                        'required' => array('enable_home_new_products', '=', '1'),
                        'title' => esc_html__('New Products Title', 'linea'),
                        'subtitle' => esc_html__('Title of New products on home page.', 'linea')
                    ), 
                     
                     array(
                            'id'=>'home_new_products_categories',
                            'type' => 'select',
                            'multi'=> true,                        
                            'data' => $cat_data,                            
                            'args' => $cat_arg,
                            'title' => esc_html__('New Product Category', 'linea'), 
                            'required' => array('enable_home_new_products', '=', '1'),
                            'subtitle' => esc_html__('Please choose New Product Category to show  its product in home page.', 'linea'),
                            'desc' => '',
                        ),

                      array(
                        'id' => 'new_products_per_page',
                        'type' => 'text',
                        'required' => array('enable_home_new_products', '=', '1'),
                        'title' => esc_html__('Number of New Products', 'linea'),
                        'subtitle' => esc_html__('Number of New products on home page.', 'linea')
                    ), 
                    

                   array(
                        'id' => 'enable_home_bestseller_products',
                        'type' => 'switch',
                        'title' => esc_html__('Show Best Seller Products', 'linea'),
                        'subtitle' => esc_html__('You can show best seller products on home page.', 'linea')
                    ),

                                     
                    array(
                        'id' => 'bestseller_per_page',
                        'type' => 'text',
                        'required' => array('enable_home_bestseller_products', '=', '1'),
                        'title' => esc_html__('Number of Bestseller Products', 'linea'),
                        'subtitle' => esc_html__('Number of Bestseller products on home page.', 'linea')
                    ), 

                    array(
                        'id' => 'enable_home_featured_products',
                        'type' => 'switch',
                        'title' => esc_html__('Show Featured Products', 'linea'),
                        'subtitle' => esc_html__('You can show featured products on home page.', 'linea')
                    ),
                    
                  
   
                    array(
                        'id' => 'featured_per_page',
                        'type' => 'text',
                        'required' => array('enable_home_featured_products', '=', '1'),
                        'title' => esc_html__('Number of Featured Products', 'linea'),
                        'subtitle' => esc_html__('Number of Featured products on home page.', 'linea')
                    ), 
                   
                   array(
                        'id' => 'enable_home_page_custom_slider',
                        'type' => 'switch',
                        'title' => esc_html__('Enable Home Page custom Slider', 'linea'),
                        'subtitle' => esc_html__('You can enable/disable Home page custom Slider', 'linea')
                    ),

                    array(
                        'id' => 'home_page_bottom_slider',
                        'type' => 'slides',
                        'title' => esc_html__('Home page custom Slider Uploads', 'linea'),
                        'required' => array('enable_home_page_custom_slider', '=', '1'),
                        'subtitle' => esc_html__('Unlimited slide uploads with drag and drop sortings.', 'linea'),
                        'placeholder' => array(
                            'title' => esc_html__('This is a title', 'linea'),
                            'description' => esc_html__('Description Here', 'linea'),
                            'url' => esc_html__('Give us a link!', 'linea'),
                        ),

                    ),


                   array(
                        'id' => 'enable_home_hotdeal_products',
                        'type' => 'switch',
                        
                        'title' => esc_html__('Show Hot Deal Product', 'linea'),
                        'subtitle' => esc_html__('You can show Hot Deal product on home page.', 'linea')
                    ),
                   
                       array(
                        'id' => 'enable_home_hotdeal_on_products_page',
                        'type' => 'switch',
                        'title' => esc_html__('Show Hot Deal on Product Detail Page', 'linea'),
                        'subtitle' => esc_html__('You can show Hot Deal on Product Detail page.', 'linea')
                    ),
                    
                    array(
                        'id'       => 'enable_testimonial',
                        'type'     => 'switch',                    
                        'title'    => esc_html__( 'Enable Testimonial ', 'linea' ),
                        'subtitle' => esc_html__( 'You can enable/disable Testimonial Uploads', 'linea' ),
                          'default' => '0'
                    ), 

                    array(
                        'id' => 'all_testimonial',
                        'type' => 'slides',
                        'required' => array('enable_testimonial', '=', '1'),
                        'title' => esc_html__('Add Testimonial here', 'linea'),
                        'subtitle' => esc_html__('Unlimited testimonial.', 'linea'),
                        'placeholder' => array(
                            'title' => esc_html__('This is a title', 'linea'),
                            'description' => esc_html__('Description Here', 'linea'),
                            'url' => esc_html__('Give us a link!', 'linea'),
                        ),
                        ),              
                    array(
                        'id' => 'enable_home_bottom_banner',
                        'type' => 'switch',
                        'title' => esc_html__('Enable Home Page bottom Banners', 'linea'),
                        'subtitle' => esc_html__('You can enable/disable Home page Slider Bottom Banners', 'linea')
                    ),

                    array(
                        'id' => 'home-bottom-banner',
                        'type' => 'media',
                        'required' => array('enable_home_bottom_banner', '=', '1'),
                        'title' => esc_html__('Home bottom Banner 1', 'linea'),
                        'desc' => esc_html__('', 'linea'),
                        'subtitle' => esc_html__('Home Sub Banner 1', 'linea'),
                    ),

                    array(
                        'id' => 'home-bottom-banner-url',
                        'type' => 'text',
                        'required' => array('enable_home_bottom_banner', '=', '1'),
                        'title' => esc_html__('Home Sub Banner 1 URL', 'linea'),
                        'subtitle' => esc_html__('Home Sub Banner 1 URL', 'linea'),
                    ),
                     array(
                        'id' => 'home-bottom-banner_title',
                        'type' => 'text',
                        'required' => array('enable_home_bottom_banner', '=', '1'),
                        'title' => esc_html__('Home bottom banner image Title', 'linea'),
                        'subtitle' => esc_html__('Home bottom banner image Title', 'linea'),
                    ),

                     array(
                        'id' => 'home-bottom-banner-text',
                        'type' => 'text',
                        'required' => array('enable_home_bottom_banner', '=', '1'),
                        'title' => esc_html__('Home bottom Banner-1 text', 'linea'),
                        'subtitle' => esc_html__('Home bottom Banner-1 text', 'linea'),
                    ),

                    array(
                        'id' => 'enable_home_related_products',
                        'type' => 'switch',
                        'title' => esc_html__('Show Related Products', 'linea'),
                        'subtitle' => esc_html__('You can show Related products on home page.', 'linea')
                    ),
                
                    array(
                        'id' => 'related_per_page',
                        'type' => 'text',
                       'required' => array('enable_home_related_products', '=', '1'), 
                        'title' => esc_html__('Number of Related Products', 'linea'),
                        'subtitle' => esc_html__('Number of Related products on home page.', 'linea')
                    ),
                    

                    array(
                        'id' => 'enable_home_upsell_products',
                        'type' => 'switch',
                        'title' => esc_html__('Show Upsell Products', 'linea'),
                        'subtitle' => esc_html__('You can show Upsell products on home page.', 'linea')
                    ),
                  
                    array(
                        'id' => 'upsell_per_page',
                        'type' => 'text',
                        'required' => array('enable_home_upsell_products', '=', '1'), 
                        'title' => esc_html__('Number of Upsell Products', 'linea'),
                        'subtitle' => esc_html__('Number of Upsell products on home page.', 'linea')
                    ),
                     
                    array(
                        'id' => 'enable_cross_sells_products',
                        'type' => 'switch',
                        'title' => esc_html__('Show cross sells Products', 'linea'),
                        'subtitle' => esc_html__('You can show cross sells products.', 'linea')
                    ),

                     array(
                        'id' => 'cross_per_page',
                        'type' => 'text',
                        'required' => array('enable_cross_sells_products', '=', '1'), 
                        'title' => esc_html__('Number of cross sells Products', 'linea'),
                        'subtitle' => esc_html__('Number of cross sells Products', 'linea')
                    ),
                       
                    array(
                        'id' => 'enable_home_blog_posts',
                        'type' => 'switch',
                        'title' => esc_html__('Show Home Blog', 'linea'),
                        'subtitle' => esc_html__('You can show latest blog post on home page.', 'linea')
                    ),

                ), // fields array ends
            ));


       // Edgesettings: General Settings Tab
    Redux::setSection( $opt_name, array(

                'icon' => 'el-icon-cogs',
                'title' => esc_html__('General Settings', 'linea'),
                'fields' => array(
                    array(
                    'id'       => 'category_pagelayout',
                    'type'     => 'radio',
                    'title'    => esc_html__('Set Category Page Default Layout', 'linea'), 
                    'subtitle' => esc_html__('Set Category Page Default Layout', 'linea'),
                    'desc'     => esc_html__('You cans set Category Page Default Layout here', 'linea'),
                    //Must provide key => value pairs for radio options
                    'options'  => array(
                        'grid' => esc_html__('Grid', 'linea'), 
                        'list' => esc_html__('List', 'linea'), 
                     
                    ),
                    'default' => 'grid'
                ),
                                                                                               
                     array(
                     'id'       => 'category_item',
                     'type'     => 'spinner', 
                     'title'    => esc_html__('Product display in product category page', 'linea'),
                     'subtitle' => esc_html__('Number of item display in product category page','linea'),
                     'desc'     => esc_html__('Number of item display in product category page', 'linea'),
                     'default'  => '9',
                     'min'      => '0',
                     'step'     => '1',
                     'max'      => '100',
                     ),               
                   
                                       
                    array(
                        'id'       => 'enable_product_socialshare',
                        'type'     => 'switch',                    
                        'title'    => esc_html__( 'Enable Product Page Social Share ', 'linea' ),
                        'subtitle' => esc_html__( 'You can enable/disable Product Page Social Share', 'linea' ),
                          'default' => '0'
                    ), 
                      
                    array(
                        'id' => 'back_to_top',
                        'type' => 'switch',
                        'title' => esc_html__('Back To Top Button', 'linea'),
                        'subtitle' => esc_html__('Toggle whether or not to enable a back to top button on your pages.', 'linea'),
                        'default' => true,
                    ),

                    array(
                        'id' => 'header_show_info_banner',
                        'type' => 'switch',
                        'title' => esc_html__('Show header service', 'linea'),
                        'default' => '0'
                    ),

                 
                    array(
                        'id' => 'shipping_banner',
                        'type' => 'text',
                        'required' => array('header_show_info_banner', '=', '1'),
                        'title' => esc_html__('Shipping Banner Text', 'linea'),
                    ),

                                        
                    array(
                        'id' => 'header_customer_support_banner',
                        'type' => 'text',
                        'required' => array('header_show_info_banner', '=', '1'),
                        'title' => esc_html__('Customer Support Banner Text', 'linea'),
                    ),
                    
                    array(
                        'id' => 'header_moneyback_banner',
                        'type' => 'text',
                        'required' => array('header_show_info_banner', '=', '1'),
                        'title' => esc_html__('Warrant/Gaurantee Banner Text', 'linea'),
                    ),

                    array(
                        'id' => 'header_returnservice_banner',
                        'type' => 'text',
                        'required' => array('header_show_info_banner', '=', '1'),
                        'title' => esc_html__('Return service Banner Text', 'linea'),
                    ),

                    

                    
                    
                )
            ) );

  // Edgesettings: General Options -> Styling Options Settings Tab
    Redux::setSection( $opt_name, array(
                'icon' => 'el-icon-website',
                'title' => esc_html__('Styling Options', 'linea'),
               
                'fields' => array(
                       

                    array(
                        'id' => 'set_body_background_img_color',
                        'type' => 'switch',
                        'title' => esc_html__('Set Body Background', 'linea'),
                        'subtitle' => esc_html__('', 'linea'),
                        'default' => 0,
                        'on' => 'On',
                        'off' => 'Off',
                    ),
                    array(
                        'id' => 'opt-background',
                        'type' => 'background',
                        'required' => array('set_body_background_img_color', '=', '1'),
                        'output' => array('body'),
                        'title' => esc_html__('Body Background', 'linea'),
                        'subtitle' => esc_html__('Body background with image, color, etc.', 'linea'),               
                        'transparent' => false,
                    ),                   
                    array(
                        'id' => 'opt-color-footer',
                        'type' => 'color',
                        'title' => esc_html__('Footer Background Color', 'linea'),
                        'subtitle' => esc_html__('Pick a background color for the footer.', 'linea'),
                        'validate' => 'color',
                        'transparent' => false,
                        'mode' => 'background',
                        'output' => array('footer','.footer-bottom')
                    ),
                    array(
                        'id' => 'opt-color-rgba',
                        'type' => 'color',
                        'title' => esc_html__('Header Nav Menu Background', 'linea'),
                        'output' => array('.mgk-main-menu'),
                        'mode' => 'background',
                        'validate' => 'color',
                        'transparent' => false,
                    ),
                    array(
                        'id' => 'opt-color-header',
                        'type' => 'color',
                        'title' => esc_html__('Header Background', 'linea'),
                        'transparent' => false,
                        'output' => array('header'),
                        'mode' => 'background',
                    ),  

                  
                      
                                    
                )
            ));
  // Edgesettings: Header Tab
    Redux::setSection( $opt_name, array(
                'icon' => 'el-icon-file-alt',
                'title' => esc_html__('Header', 'linea'),
                'heading' => esc_html__('All header related options are listed here.', 'linea'),
                'desc' => esc_html__('', 'linea'),
                'fields' => array(
                    array(
                        'id' => 'enable_header_currency',
                        'type' => 'switch',
                        'title' => esc_html__('Show Currency HTML', 'linea'),
                        'subtitle' => esc_html__('You can show Currency in the header.', 'linea')
                    ),
                    array(
                        'id' => 'enable_header_language',
                        'type' => 'switch',
                        'title' => esc_html__('Show Language HTML', 'linea'),
                        'subtitle' => esc_html__('You can show Language in the header.', 'linea')
                    ),
                    array(
                        'id' => 'enable_mini_cart',
                        'type' => 'switch',
                        'title' => esc_html__('Show Mini Cart', 'linea'),
                        'subtitle' => esc_html__('You can enable/disable Mini Cart', 'linea')
                    ),
                    array(
                        'id' => 'header_use_imagelogo',
                        'type' => 'checkbox',
                        'title' => esc_html__('Use Image for Logo?', 'linea'),
                        'subtitle' => esc_html__('If left unchecked, plain text will be used instead (generated from site name).', 'linea'),
                        'desc' => esc_html__('', 'linea'),
                        'default' => '1'
                    ),
                    array(
                        'id' => 'header_logo',
                        'type' => 'media',
                        'required' => array('header_use_imagelogo', '=', '1'),
                        'title' => esc_html__('Logo Upload', 'linea'),
                        'desc' => esc_html__('', 'linea'),
                        'subtitle' => esc_html__('Upload your logo here and enter the height of it below', 'linea'),
                    ),
                    array(
                        'id' => 'header_logo_height',
                        'type' => 'text',
                        'required' => array('header_use_imagelogo', '=', '1'),
                        'title' => esc_html__('Logo Height', 'linea'),
                        'subtitle' => esc_html__('Don\'t include "px" in the string. e.g. 30', 'linea'),
                        'desc' => '',
                        'validate' => 'numeric'
                    ),
                    array(
                        'id' => 'header_logo_width',
                        'type' => 'text',
                        'required' => array('header_use_imagelogo', '=', '1'),
                        'title' => esc_html__('Logo Width', 'linea'),
                        'subtitle' => esc_html__('Don\'t include "px" in the string. e.g. 30', 'linea'),
                        'desc' => '',
                        'validate' => 'numeric'
                    ),    
                                 
                    array(
                        'id' => 'header_remove_header_search',
                        'type' => 'checkbox',
                        'title' => esc_html__('Remove Header Search', 'linea'),
                        'subtitle' => esc_html__('Active to remove the search functionality from your header', 'linea'),
                        'desc' => esc_html__('', 'linea'),
                        'default' => '0'
                    ),
                   
                  array(
            'id' => 'header_show_ajax_search',
            'type' => 'checkbox',
            'title' => esc_html__('Enable Ajax Search', 'linea'),
            'subtitle' => esc_html__('Header ajax search active only when magik wooajax search plugin activated and option enable', 'linea'),
            'desc' => esc_html__('', 'linea'),
            'default' => '0'
        )
                   
                ) //fields end
            ));
      // Edgesettings: Menu Tab
    Redux::setSection( $opt_name, array(
                'icon' => 'el el-website icon',
                'title' => esc_html__('Menu', 'linea'),
                'heading' => esc_html__('All Menu related options are listed here.', 'linea'),
                'desc' => esc_html__('', 'linea'),
                'fields' => array(

                  array(
                        'id' => 'show_menu_arrow',
                        'type' => 'switch',
                        'title' => esc_html__('Show Menu Arrow', 'linea'),
                        'desc'  => esc_html__('Show arrow in menu.', 'linea'),
                        
                    ),               
                   array(
                    'id'       => 'login_button_pos',
                    'type'     => 'radio',
                    'title'    => esc_html__('Show Login/sign and logout link', 'linea'),                   
                    'desc'     => esc_html__('Please Select any option from above.', 'linea'),
                     //Must provide key => value pairs for radio options
                    'options'  => array(
                    'none' => 'None', 
                   'toplinks' => 'In Top Menu', 
                   'main_menu' => 'In Main Menu'
                    ),
                   'default' => 'none'
                    )
                  
                ) // fields ends here
            ));

 // Edgesettings: Footer Tab
    Redux::setSection( $opt_name, array(
                'icon' => 'el-icon-file-alt',
                'title' => esc_html__('Footer', 'linea'),
                'heading' => esc_html__('All footer related options are listed here.', 'linea'),
                'desc' => esc_html__('', 'linea'),
                'fields' => array(
                     array(
                        'id'       => 'enable_mailchimp_form',
                        'type'     => 'switch',                    
                        'title'    => esc_html__( 'Enable Mailchimp Form', 'linea' ),
                        'subtitle' => esc_html__( 'You can enable/disable Mailchimp Form', 'linea' ),
                          'default' => '0'
                    ), 
                    array(
                        'id' => 'footer_color_scheme',
                        'type' => 'switch',
                        'title' => esc_html__('Custom Footer Color Scheme', 'linea'),
                        'subtitle' => esc_html__('', 'linea')
                    ),               
                    array(
                        'id' => 'footer_copyright_background_color',
                        'type' => 'color',
                        'required' => array('footer_color_scheme', '=', '1'),
                        'transparent' => false,
                        'title' => esc_html__('Footer Copyright Background Color', 'linea'),
                        'subtitle' => esc_html__('', 'linea'),
                        'validate' => 'color',
                    ),
                    array(
                        'id' => 'footer_copyright_font_color',
                        'type' => 'color',
                        'required' => array('footer_color_scheme', '=', '1'),
                        'transparent' => false,
                        'title' => esc_html__('Footer Copyright Font Color', 'linea'),
                        'subtitle' => esc_html__('', 'linea'),
                        'validate' => 'color',
                    ), 
                    
                    array(
                        'id' => 'enable_footer_middle',
                        'type' => 'switch',                       
                        'title' => esc_html__('Enable footer middle', 'linea'),
                        'subtitle' => esc_html__('You can enable/disable Footer Middle', 'linea')
                    ),

                    array(
                        'id' => 'footer_middle',
                        'type' => 'editor',
                        'title' => esc_html__('Footer Middle Text ', 'linea'), 
                        'required' => array('enable_footer_middle', '=', '1'),               
                       'subtitle' => esc_html__('You can use the following shortcodes in your footer text: [wp-url] [site-url] [theme-url] [login-url] [logout-url] [site-title] [site-tagline] [current-year]', 'linea'),
                        'default' => '',
                    ), 
                                            
                    array(
                        'id' => 'bottom-footer-text',
                        'type' => 'editor',
                        'title' => esc_html__('Bottom Footer Text', 'linea'),
                        'subtitle' => esc_html__('You can use the following shortcodes in your footer text: [wp-url] [site-url] [theme-url] [login-url] [logout-url] [site-title] [site-tagline] [current-year]', 'linea'),
                        'default' => esc_html__('Powered by Magik', 'linea'),
                    ),
                    
                    
                ) // fields ends here
            ));

       //Edgesettings: Blog Tab
    Redux::setSection( $opt_name,  array(
                'icon' => 'el-icon-pencil',
                'title' => esc_html__('Blog Page', 'linea'),
                'fields' => array( 
                       array(
                        'id' => 'blog-page-layout',
                        'type' => 'image_select',
                        'title' => esc_html__('Blog Page Layout', 'linea'),
                        'subtitle' => esc_html__('Select main blog listing and category page layout from the available blog layouts.', 'linea'),
                        'options' => array(
                            '1' => array(
                                'alt' => 'Left sidebar',
                                'img' => LINEA_THEME_URI . '/images/magik_col/category-layout-1.png'

                            ),
                            '2' => array(
                                'alt' => 'Right Right',
                                'img' => LINEA_THEME_URI . '/images/magik_col/category-layout-2.png'
                            ),
                            '3' => array(
                                'alt' => '2 Column Right',
                                'img' => LINEA_THEME_URI . '/images/magik_col/category-layout-3.png'
                            )                                                                                 
                          
                        ),
                        'default' => '2'
                    ), 
                     array(
                        'id' => 'blog_show_authors_bio',
                        'type' => 'switch',
                        'title' => esc_html__('Author\'s Bio', 'linea'),
                        'subtitle' => esc_html__('Show Author Bio on Blog page.', 'linea'),
                         'default' => true,
                        'desc' => esc_html__('', 'linea')
                    ),                  
                    array(
                        'id' => 'blog_show_post_by',
                        'type' => 'switch',
                        'title' => esc_html__('Display Post By', 'linea'),
                         'default' => true,
                        'subtitle' => esc_html__('Display Psot by Author on Listing Page', 'linea')
                    ),
                    array(
                        'id' => 'blog_display_tags',
                        'type' => 'switch',
                        'title' => esc_html__('Display Tags', 'linea'),
                         'default' => true,
                        'subtitle' => esc_html__('Display tags at the bottom of posts.', 'linea')
                    ),
                    array(
                        'id' => 'blog_full_date',
                        'type' => 'switch',
                        'title' => esc_html__('Display Full Date', 'linea'),
                        'default' => true,
                        'subtitle' => esc_html__('This will add date of post meta on all blog pages.', 'linea')
                    ),
                    array(
                        'id' => 'blog_display_comments_count',
                        'type' => 'switch',
                        'default' => true,
                        'title' => esc_html__('Display Comments Count', 'linea'),
                        'subtitle' => esc_html__('Display Comments Count on Blog Listing.', 'linea')
                    ),
                    array(
                        'id' => 'blog_display_category',
                        'type' => 'switch',
                        'title' => esc_html__('Display Category', 'linea'),
                         'default' => true,
                        'subtitle' => esc_html__('Display Comments Category on Blog Listing.', 'linea')
                    ),
                    array(
                        'id' => 'blog_display_view_counts',
                        'type' => 'switch',
                        'title' => esc_html__('Display View Counts', 'linea'),
                         'default' => true,
                        'subtitle' => esc_html__('Display View Counts on Blog Listing.', 'linea')
                    ),                  
                )
            ));
  // Edgesettings: Social Media Tab
    Redux::setSection( $opt_name, array(
                'icon' => 'el-icon-file',
                'title' => esc_html__('Social Media', 'linea'),
                'fields' => array(
                     array(
                        'id'       => 'enable_social_link_footer',
                        'type'     => 'switch',                    
                        'title'    => esc_html__( 'Enable Social Link In Footer', 'linea' ),                        
                        'default' => '0'
                    ),
                    array(
                        'id' => 'social_facebook',
                        'type' => 'text',
                        'title' => esc_html__('Facebook URL', 'linea'),
                        'subtitle' => esc_html__('Please enter in your Facebook URL.', 'linea'),
                    ),
                    array(
                        'id' => 'social_twitter',
                        'type' => 'text',
                        'title' => esc_html__('Twitter URL', 'linea'),
                        'subtitle' => esc_html__('Please enter in your Twitter URL.', 'linea'),
                    ),
                    array(
                        'id' => 'social_googlep',
                        'type' => 'text',
                        'title' => esc_html__('Google+ URL', 'linea'),
                        'subtitle' => esc_html__('Please enter in your Google Plus URL.', 'linea'),
                    ),
                  
                    array(
                        'id' => 'social_pinterest',
                        'type' => 'text',
                        'title' => esc_html__('Pinterest URL', 'linea'),
                        'subtitle' => esc_html__('Please enter in your Pinterest URL.', 'linea'),
                    ),
                    array(
                        'id' => 'social_youtube',
                        'type' => 'text',
                        'title' => esc_html__('Youtube URL', 'linea'),
                        'subtitle' => esc_html__('Please enter in your Youtube URL.', 'linea'),
                    ),
                    array(
                        'id' => 'social_linkedin',
                        'type' => 'text',
                        'title' => esc_html__('LinkedIn URL', 'linea'),
                        'subtitle' => esc_html__('Please enter in your LinkedIn URL.', 'linea'),
                    ),
                    array(
                        'id' => 'social_instagram',
                        'type' => 'text',
                        'title' => esc_html__('Instagram URL', 'linea'),
                        'subtitle' => esc_html__('Please enter in your Instagram URL.', 'linea'),
                    ),
                    array(
                        'id' => 'social_rss',
                        'type' => 'text',
                        'title' => esc_html__('RSS URL', 'linea'),
                        'subtitle' => esc_html__('Please enter in your RSS URL.', 'linea'),
                    )                   
                )
            ));

   
 
    /*
     * <--- END SECTIONS
     */

