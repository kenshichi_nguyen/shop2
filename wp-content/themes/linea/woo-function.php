<?php
    remove_action('woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10, 0);
    remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0);
    remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5);
    remove_action('woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15);
    remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);
   
    remove_action('woocommerce_proceed_to_checkout', 'woocommerce_button_proceed_to_checkout',20);
    remove_action('woocommerce_cart_collaterals', 'woocommerce_cart_totals');
    remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5);
       
    
    add_action('woocommerce_after_single_product_summary', 'linea_related_upsell_products', 15);
    add_action('woocommerce_after_shop_loop_item_title', 'linea_woocommerce_rating', 5);
    add_action('woocommerce_before_shop_loop', 'linea_grid_list_trigger', 11);
     
    add_action('woocommerce_proceed_to_checkout', 'linea_woocommerce_button_proceed_to_checkout');
    add_action('init','linea_woocommerce_clear_cart_url');
    add_action('linea_single_product_pagination', 'linea_single_product_prev_next');
    add_filter('woocommerce_breadcrumb_defaults','linea_woocommerce_breadcrumbs');
    add_filter('woocommerce_add_to_cart_fragments', 'linea_woocommerce_header_add_to_cart_fragment');
    add_filter('loop_shop_per_page', 'linea_loop_product_per_page');
    add_action( 'woocommerce_product_options_advanced', 'linea_woocommerce_general_product_data_custom_field' );
    add_action( 'woocommerce_process_product_meta', 'linea_woocommerce_process_product_meta_fields_save' );
   
    add_filter( 'woocommerce_cross_sells_total', 'linea_woocommerce_cross_sells_total_limit' );

function linea_related_upsell_products()
{
    global $product,$linea_Options;

    if (isset($product) && is_product())
     {
     ?>
     
             
      <?php if (isset($linea_Options['enable_home_related_products']) && !empty($linea_Options['enable_home_related_products']) ) 
     { ?>
 <!-- Related Slider -->
 <div class="related-pro">
   <div class="slider-items-products">
      <div class="related-block">
          <div class="home-block-inner">
              <div class="block-title">
                 <h2><?php esc_attr_e('Related Products', 'linea'); ?></h2>
              </div>
          </div>

      <div id="related-products-slider" class="product-flexslider hidden-buttons">
        <div class="slider-items slider-width-col4 products-grid block-content">
          <?php
                            $related = wc_get_related_products(6);
                            $args = apply_filters('woocommerce_related_products_args', array(
                                'post_type' => 'product',
                                'ignore_sticky_posts' => 1,
                                'no_found_rows' => 1,
                                'posts_per_page' => $linea_Options['related_per_page'],
                                'orderby' => 'rand',
                                'post__in' => $related,
                                'post__not_in' => array($product->get_id())
                            ));

                            $loop = new WP_Query($args);
                            if ($loop->have_posts()) {
                                while ($loop->have_posts()) : $loop->the_post();
                                   linea_related_upsell_template();
                                endwhile;
                            } else {
                                esc_attr_e('No products found', 'linea');
                            }

                            wp_reset_postdata();
                            ?>
         
            </div>
          </div>

        </div>
      </div>
    </div>
  
<?php } ?>
  <!-- End related products Slider -->
       
            
<?php
      if (isset($linea_Options['enable_home_upsell_products']) && !empty($linea_Options['enable_home_upsell_products']) ) 
     { 


        $upsells = $product->get_upsell_ids();

        if (sizeof($upsells) == 0) {
        } else {
            ?>
  <!-- upsell Slider -->
  <div class="upsell-pro">
    <div class="slider-items-products">
       <div class="upsell-block">
          <div class="home-block-inner">
              <div class="block-title">
                  <h2><?php esc_attr_e('Upsell Product', 'linea'); ?></h2>
              </div>
          </div>
      <div id="upsell-products-slider" class="product-flexslider hidden-buttons">
        <div class="slider-items slider-width-col4 products-grid block-content">  

          <?php

                                $meta_query = WC()->query->get_meta_query();

                                $args = array(
                                    'post_type' => 'product',
                                    'ignore_sticky_posts' => 1,
                                    'no_found_rows' => 1,
                                    'posts_per_page' => $linea_Options['upsell_per_page'],
                                    'orderby' => 'rand',
                                    'post__in' => $upsells,
                                    'post__not_in' => array($product->get_id()),
                                    'meta_query' => $meta_query
                                );


                                $loop = new WP_Query($args);
                                if ($loop->have_posts()) {
                                    while ($loop->have_posts()) : $loop->the_post();
                                       linea_related_upsell_template();
                                    endwhile;
                                } else {
                                    esc_attr_e('No products found', 'linea');
                                }

                                wp_reset_postdata();
                                ?>

            </div>
          </div>

      </div>
    </div>
  </div>
         
<?php
   }

 }?>

<?php
    }
}


function linea_woocommerce_rating()
{
    global $wpdb, $post;

    $count = $wpdb->get_var($wpdb->prepare("
        SELECT COUNT(meta_value) FROM $wpdb->commentmeta
        LEFT JOIN $wpdb->comments ON $wpdb->commentmeta.comment_id = $wpdb->comments.comment_ID
        WHERE meta_key = '%s'
        AND comment_post_ID = %d
        AND comment_approved = %d
        AND meta_value > %d
    ", 'rating', $post->ID, 1, 0));

    $rating = $wpdb->get_var($wpdb->prepare("
        SELECT SUM(meta_value) FROM $wpdb->commentmeta
        LEFT JOIN $wpdb->comments ON $wpdb->commentmeta.comment_id = $wpdb->comments.comment_ID
        WHERE meta_key = '%s'
        AND comment_post_ID = %d
        AND comment_approved = %d
    ", 'rating', $post->ID, 1));

    if ($count > 0) {

        $average = number_format($rating / $count, 2);

        echo '<div class="ratings" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">';

        echo '<div class="rating-box">';

        echo '<div class="rating" title="' . sprintf(esc_html__('Rated %s out of 5', 'woocommerce'), esc_html($average)) . '"  style="width:' . esc_html($average * 16) . 'px"><span itemprop="ratingValue" class="rating">' . esc_html($average) . '</span></div>';

        echo '</div></div>';
    }


}


function linea_grid_list_trigger()
{
    ?>

<div class="sorter">
  <div class="view-mode"><a href="#" class="grid-trigger button-active button-grid"></a> <a href="#" title="<?php esc_attr_e('List', 'linea'); ?>" class="list-trigger  button-list"></a></div>
</div>
<?php

}



function linea_woocommerce_add_to_whishlist()
{
    global $product, $yith_wcwl;

    if (isset($yith_wcwl) && is_object($yith_wcwl)) {
        $classes = get_option('yith_wcwl_use_button') == 'yes' ? 'class="link-wishlist"' : 'class="link-wishlist"';
        ?>
 <li class="pull-left-none"><a href="<?php echo esc_url($yith_wcwl->get_addtowishlist_url()) ?>"
           data-product-id="<?php echo esc_html($product->get_id()); ?>"
           data-product-type="<?php echo esc_html($product->get_type()); ?>" <?php echo wp_specialchars_decode($classes); ?>
           title="<?php esc_attr_e('Add to WishList','linea'); ?>"><i class="fa fa-heart-o icons"></i><?php esc_attr_e('Add to WishList','linea'); ?></a></li>
<?php
    }
}


function linea_woocommerce_button_proceed_to_checkout()
{
    $checkout_url = wc_get_checkout_url();
    ?>
    <a href="<?php echo esc_url($checkout_url); ?>" class="button btn-proceed-checkout">
        <span><?php esc_attr_e('Proceed to Checkout', 'linea'); ?></span></a>
<?php
}

function linea_woocommerce_clear_cart_url()
{
    global $woocommerce;
    if (isset($_REQUEST['clear-cart'])) {
        $woocommerce->cart->empty_cart();
    }
}

//Filter function are here
/* Breadcrumbs */

function linea_woocommerce_breadcrumbs()
{
    return array(
        'delimiter' => '<span> &frasl; </span>',
        'wrap_before' => '<ul>',
        'wrap_after' => '</ul>',
        'before' => '<li>',
        'after' => '</li>',
        'home' => _x('Home', 'breadcrumb', 'linea'),
    );
}

function linea_single_product_prev_next()
  {

    global $woocommerce, $post;

    if (!isset($woocommerce) or !is_single())
      return;
    ?>
  <div id="prev-next" class="product-next-prev">
    <?php
      $next =linea_prev_next_product_object($post->ID);

      if (!empty($next)):
        ?>
    <a href="<?php echo esc_url(get_permalink($next->ID)) ?>" class="product-next"><span></span></a>
    <?php
      endif;

      $prev = linea_prev_next_product_object($post->ID, 'prev');
      if (!empty($prev)):
        ?>
    <a href="<?php echo esc_url(get_permalink($prev->ID)) ?>" class="product-prev"><span></span></a>
    <?php
      endif;
      ?>
  </div>
  <!--#prev-next -->

  <?php
  }
function linea_prev_next_product_object($postid, $dir = 'next')
  {

    global $wpdb;
    
    if ($dir == 'prev')
      $sql = $wpdb->prepare("SELECT * FROM $wpdb->posts where post_type = '%s' AND post_status = '%s' and ID < %d order by ID desc limit 0,1", 'product', 'publish', $postid);
    else
      $sql = $wpdb->prepare("SELECT * FROM $wpdb->posts where post_type = '%s' AND post_status = '%s' and ID > %d order by ID desc limit 0,1", 'product', 'publish', $postid);

    $result = $wpdb->get_row($sql);

    if (!is_wp_error($result)):
      if (!empty($result)):
        return $result;
      else:
        return false;
      endif;
    else:
      return false;
    endif;
  }


function linea_woocommerce_header_add_to_cart_fragment( $fragments ) {
    global $woocommerce,$linea_Options;

ob_start();
?>

<div class="mini-cart">
             <div  data-hover="dropdown"  class="basket dropdown-toggle">
              <a href="<?php echo esc_url(wc_get_cart_url()); ?>"> 
                <span class="price hidden-xs"><?php  esc_attr_e('Shopping Cart','linea'); ?></span> 
                <span class="cart_count hidden-xs"><?php echo esc_html($woocommerce->cart->cart_contents_count); ?> <?php  esc_attr_e('Items','linea'); ?>/ <?php echo wp_specialchars_decode(WC()->cart->get_cart_subtotal()); ?></span>
               </a>
            </div>

    <div>     
        <div class="top-cart-content">
                
    
                   
         <?php if (sizeof(WC()->cart->get_cart()) > 0) : $i = 0; ?>
         <ul class="mini-products-list" id="cart-sidebar" >
            <?php foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) : ?>
          <?php
               $_product = apply_filters('woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key);
               $product_id = apply_filters('woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key);
               
               if ($_product && $_product->exists() && $cart_item['quantity'] > 0
                   && apply_filters('woocommerce_widget_cart_item_visible', true, $cart_item, $cart_item_key)
               ) :
               
                   $product_name      = apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key );
                 $thumbnail         = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );
                   $product_price     = apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
          $product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
                   $cnt = sizeof(WC()->cart->get_cart());
                   $rowstatus = $cnt % 2 ? 'odd' : 'even';
                   ?>
            <li class="item<?php if ($cnt - 1 == $i) { ?>last<?php } ?>">
              <div class="item-inner">
               <a class="product-image"
                  href="<?php echo esc_url($product_permalink); ?>"  title="<?php echo esc_html($product_name); ?>"> <?php echo str_replace(array('http:', 'https:'), '', wp_specialchars_decode($thumbnail)); ?> </a>
             

                  <div class="product-details">
                       <div class="access">
                        <a class="btn-edit" title="<?php esc_attr_e('Edit item','linea') ;?>"
                        href="<?php echo esc_url(wc_get_cart_url()); ?>"><i
                        class="icon-pencil"></i><span
                        class="hidden"><?php esc_attr_e('Edit item','linea') ;?></span></a>
                     <a href="<?php echo esc_url(wc_get_cart_remove_url($cart_item_key)); ?>"
                        title="<?php esc_attr_e('Remove This Item','linea') ;?>" onClick="" 
                        class="btn-remove1"><?php esc_attr_e('Remove','linea') ;?></a> 

                         </div>
                      <strong><?php echo esc_html($cart_item['quantity']); ?>
                  </strong> x <span class="price"><?php echo wp_specialchars_decode($product_price); ?></span>
                     <p class="product-name"><a href="<?php echo esc_url($product_permalink); ?>"
                        title="<?php echo esc_html($product_name); ?>"><?php echo esc_html($product_name); ?></a> </p>
                  </div>
                                  <?php echo wc_get_formatted_cart_item_data( $cart_item ); ?>

                     </div>
              
            </li>
            <?php endif; ?>
            <?php $i++; endforeach; ?>
         </ul> 
         <!--actions-->
                    
         <div class="actions">
                      <button class="btn-checkout" title="<?php esc_attr_e('Checkout','linea') ;?>" type="button" 
                      onClick="window.location.assign('<?php echo esc_js(wc_get_checkout_url()); ?>')">
                      <span><?php esc_attr_e('Checkout','linea') ;?></span> </button>

                       
                      <a class="view-cart" type="button"
                     onClick="window.location.assign('<?php echo esc_js(wc_get_cart_url()); ?>')">
                     <span><?php esc_attr_e('View Cart','linea') ;?></span> </a>
                     
          
         </div>   
         
         <?php else:?>
         <p class="a-center noitem">
            <?php esc_attr_e('Sorry, nothing in cart.', 'linea');?>
         </p>
         <?php endif; ?>
      </div>
   </div>
 </div>

<?php

 $fragments['.mini-cart'] = ob_get_clean();

return $fragments;

}

function linea_loop_product_per_page() {
    global $linea_Options;

    // replace it with theme option
    if (isset($linea_Options['category_item']) && !empty($linea_Options['category_item'])) {
        $per_page = explode(',', $linea_Options['category_item']);
    } else {
        $per_page = explode(',', '8,16,32');
    }

    $item_count = !empty($params['count']) ? $params['count'] : $per_page[0];

    return $item_count;
}

// Display Fields using WooCommerce Action Hook
function linea_woocommerce_general_product_data_custom_field() {
   global $woocommerce, $post;
   
    woocommerce_wp_checkbox(
                array(
                    'id' => 'hotdeal_on_productpage',
                    'wrapper_class' => 'checkbox_class',
                    'label' => esc_html__('Hot deal on Product Detail Page', 'linea' ),
                    'description' => esc_html__( 'Tick checkbox to set product as hot deal on Product Deatil Page', 'linea' )
                )
            );
    woocommerce_wp_checkbox(
                array(
                    'id' => 'hotdeal_on_home',
                    'wrapper_class' => 'checkbox_class',
                    'label' => esc_html__('Hot deal Product', 'woocommerce' ),
                    'description' => esc_html__( 'Tick checkbox to set product as hot deal product', 'woocommerce' )
                )
            );
    
    
}

// Save Fields using WooCommerce Action Hook
function linea_woocommerce_process_product_meta_fields_save( $post_id ){
    global  $woo_checkbox;
      $woo_checkbox = isset( $_POST['hotdeal_on_productpage'] ) ? 'yes' : 'no';
    update_post_meta( $post_id, 'hotdeal_on_productpage', sanitize_text_field($woo_checkbox ));
    $woo_checkbox = isset( $_POST['hotdeal_on_home'] ) ? 'yes' : 'no';
    update_post_meta( $post_id, 'hotdeal_on_home',sanitize_text_field( $woo_checkbox ));

}

//cross sells limit
function linea_woocommerce_cross_sells_total_limit() {
    global $linea_Options;
    if (isset($linea_Options['enable_cross_sells_products']) && !empty($linea_Options['enable_cross_sells_products'])) {
       $cross_sells_per_page = $linea_Options['cross_per_page'];
      return $cross_sells_per_page;
     }
}



 add_action('woocommerce_single_product_summary', 'linea_hot_detail_on_detailpage', 15);

 function linea_hot_detail_on_detailpage()
 {

global $product , $linea_Options;

if (isset($linea_Options['enable_home_hotdeal_on_products_page']) && !empty($linea_Options['enable_home_hotdeal_on_products_page'])) { 
   $hotdeal_value = get_post_meta($product->get_id(), 'hotdeal_on_productpage', true);
if($hotdeal_value=="yes" && $product->is_on_sale())
{ 
  $product_type = $product->get_type();
            
              if($product_type=='variable')
              {
               $available_variations = $product->get_available_variations();
             
               $variation_id=$available_variations[0]['variation_id'];
                $newid=$variation_id;
              }
              else
              {
                $newid=$product->get_id();
           
              }
                $sales_price_start =  ( $date = get_post_meta( $newid, '_sale_price_dates_from', true ) ) ? date_i18n( 'Y-m-d', $date ) : ''; 
                $sales_price_start=strtotime($sales_price_start);

                $new_end_date =  ( $date = get_post_meta( $newid, '_sale_price_dates_to', true ) ) ? date_i18n( 'Y-m-d', $date ) : ''; 
            
                $sales_price_end=strtotime($new_end_date);
            
               $curdate=strtotime(date('Y-m-d H:i:s'));

                $sales_price_to = get_post_meta($newid, '_sale_price_dates_to', true);  
               if(!empty($sales_price_to) && $sales_price_start <= $curdate  && $sales_price_end >= $curdate)
               {
              $sales_price_date_to = date("Y/m/d H:i:s", strtotime($new_end_date));
               
              
               ?>
               <div class="product-timer-box">
                <div class="box-timer">
                  <div class="countbox_2 timer-grid"  data-time="<?php echo esc_html($sales_price_date_to) ;?>">
                </div>
            </div>
            </div>
            <?php
          }
}
}
}

 ?>