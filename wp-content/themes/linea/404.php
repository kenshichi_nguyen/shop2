<?php

get_header(); ?>
  

<div class="content-wrapper">
    <div class="container">
       
        <div class="std">
            <div class="page-not-found wow bounceInRight animated">
                <h2><?php  esc_attr_e('404','linea') ;?></h2>

                <h3><img src="<?php echo esc_url(LINEA_THEME_URI) . '/images/signal.png'; ?>"
                         alt="<?php  esc_attr_e('404! Page Not Found','linea') ;?>">
                         <?php  esc_attr_e('Oops! The Page you requested was not found!','linea') ;?></h3>

                <div><a href="<?php echo esc_url(get_home_url()); ?>" type="button"
                        class="btn-home"><span><?php  esc_attr_e('Back To Home','linea') ;?></span></a></div>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>

